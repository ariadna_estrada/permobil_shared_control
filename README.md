permobil_shared_control
=====================

This repository is based on the code for wheelchair automation available at: https://github.com/OSUrobotics/wheelchair-automation.git 

Our modifications aim to enable collaboration between the user and the autnonomous planner instead of fully automating the PERMOBIL wheelchair.


Package Descriptions
---------------------

wheelchair_description: URDF and launch files for wheelchair initial bringup.

wheelchair_navigation: AMCL and Navigation parameters for the wheelchair.

wheelchair_mapping: Basic hector_mapping launch file. 


Useful launch files
---------------------

Make sure you plug in the hardware in the following order:

1. Turn on the power for both lasers
2. on laptop USB port: plug in Arduino cable
3. on laptop USB port: plug in Left laser cable
4. on laptop USB port: plug in Right laser cable
5. at the end of left arm rest: turn on the OMNI using the OMNI power button

To create a map:

1. roslaunch JoyControl wheelchair_test_sc.launch
2. roslaunch wheelchair_mapping wheelchair_hector_mapping.launch
3. save map using: rosrun map_server map_saver -f "map_name"

To test localization (amcl):

1. roslaunch JoyControl wheelchair_test_sc.launch
2. roslaunch wheelchair_navigation wheelchair_localization.launch
3. roslaunch ira_laser_tools wheelchair_scan_merger.launch
4. In matlab: test_connection.m

To test Eband Planner: 

1. roslaunch JoyControl wheelchair_shared_control.launch
2. roslaunch wheelchair_navigation wheelchair_eband.launch
3. roslaunch ira_laser_tools wheelchair_scan_merger.launch
4. In matlab: testEband_planner.m 
5. Send navigation goals using Rviz

To test simple drive (no policies)

1. roslaunch JoyControl wheelchair_test_sc.launch
2. In matlab: test_connection.m 

To run experiment setup (randomized trial order)
1. Before launching, connect to the PS3 remote controller using 'sudo sixad -s' 
2. roslaunch JoyControl wheelchair_shared_control.launch
3. roslaunch wheelchair_navigation wheelchair_sc_experiment.launch
4. roslaunch ira_laser_tools wheelchair_scan_merger.launch
5. In matlab: run_experiment.m (Open in GUIDE and play)
6. Select participant number and trial block. Press "rosinit" followed by "Begin trial"

Package Dependencies:
---------------------

In order to successfully build this package through 'catkin_make', all of the following packages should
already be installed on your system:

rospy;	
roscpp;	
std_msgs;	
sensor_msgs;	
actionlib_msgs;	
geometry_msgs;	
tf;	
message_generation;	
pcl_conversions;	
pcl_ros;	
geometry_msgs;	
hector_mapping;	
move_base;	
amcl;	
map_server;	
hokuyo_node;	
eband_local_planner;	
video_stream_opencv;	

Or try 'catkin_make' first, follow the prompts and install missing packages with 'sudo apt-get install xxx'

Additionally, for the JoyControl Package to properly build: 

roscpp;	
rospy;	
std_msgs;	
nav_msgs;	
sensor_msgs;	
message_generation;	
rosserial_python;	
joy;	

Error Handling
---------------------
Laser Failure code '33': 

1. Shut down the hokuyo node
2. Disconnect the laser power. Turn on laser left first then laser right. 
3. Repeat the launching process from start

Laser Failure code '98':
Wait for automatic recovery OR relaunch until the error is gone.

OMNI Joystick error 0808:
Turn off the OMNI and turn it back on after 3 seconds
