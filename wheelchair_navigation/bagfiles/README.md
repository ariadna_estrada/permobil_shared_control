Recording a rosbag
==================
When performing a trial run you might want to store the sensor data. Follow the next steps:

1. Start all launch files from the navigation framework. 

	T1: roslaunch JoyControl wheelchair_shared_control.launch

	T2: roslaunch wheelchair_navigation wheelchair_sc_experiment.launch

	T3: roslaunch ira_laser_tools wheelchair_scan_merger.launch

	Matlab: run_experiment.m (open in GUIDE and play). Configure experiment conditions and click "Begin Trial"
	

2. Open a terminal in this directory and run ./bag_topics. This file contains the topics that we're more interested in storing from the shared-control experiment, but it can be modified.

3. Once the trial has been completed stop the recording with Ctrl + C. 

Playing a rosbag
===========================
1. T1: roslaunch wheelchair_navigation view_trial bag_name:="full_path_to_your_bag"


Extract images from bag and run emotion detection
============================
Dependencies: affectiva-sdk, mencoder (sudo apt-get install mencoder)


1. Create a directory to store your bag. For example trial_bag_0. Change the number to match your trials.

2. Move your rosbag to your newly created directory.

3. Copy the file "process_images.sh" into the directory where your bag is stored.

4. Open a terminal in such directory

5. run ./process_images.sh "your_bag_name" 

6. Verify that two files were created in your directory: output.avi and output.csv. 


The image extraction will begin on pause. Press the space bar to continue. You can remove the process_images.sh script from the bag directory once finished.


Extracting bag in Matlab
========================
1. Launch Matlab and add to the path the 'src' directory of this package. (Including subfolders)

2. Navigate to the directory of your test_bag. Remember, there should only be one bag per directory. 

3. Run the script get_messages_from_bag.m

4. Two mat files should appear in your bag directory: trial_data.mat and emotion_data.mat

Note: This step has been tested in Matlab R2016b. For some reason this step fails when using Matlab R2018a. 

Visualizing the data
====================
1. Run Matlab script view_sc_trials.m. Make sure to specify the path to your test_bag so you load the correct mat files. 

2. Click on any axes to load the face image at that moment. 
