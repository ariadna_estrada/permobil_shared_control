Recording a rosbag
==================
When performing a trial run you might want to store the sensor data. Follow the next steps:

1. Start all launch files from the navigation framework. 

	T1: roslaunch JoyControl wheelchair_shared_control.launch

	T2: roslaunch wheelchair_navigation wheelchair_sc_experiment.launch

	T3: roslaunch ira_laser_tools wheelchair_scan_merger.launch

	Matlab: SC_wCallbacks.m
	

2. Open a terminal in this directory and run ./bag_topics. This file contains the topics that we're more interested in storing although it can be modified.

Extracting images from a bag
============================
As you might have noticed from the bag_topics file, we're only storing compressed images. We need to extract them before we can use them. 

1. roslaunch wheelchair_navigation extract_images.launch bag_name:="your_trial_bag" 

replace "your_trial_bag" with the bag file you want to use

2. The rosbag will start paused. Press the space bar to begin playing the bag.

The extracted images will be in the ~/.ros directory. To move them create a directory under the bagfiles folder and execute the following command. 

3. mv ~/.ros/frame* your_directory 

Creating a video from the images
================================ 
The Affectiva SDK needs a video file to work. 

If you haven't already, install mencoder:

1. sudo apt-get install mencoder

Open a terminal in the folder where your extracted frames are stored. Execute the following to create the video using the extracted frames. The generated video will be stored in the same directory. 

2. mencoder "mf://*.jpg" -mf type=jpg:fps=20 -o output.avi -speed 1 -ofps 20 -ovc lavc -lavcopts vcodec=mpeg4:mbd=2:trell -oac copy -o output.avi

Running the affectiva sdk
==========================
Now that you have the video locate the cpp-sdk-examples directory. Go to build/video_demo and run the following command:

1. ./video-demo --input path_to_your_video_file --data ~/affdex-sdk/data --faceMode 0

The generated csv file will be in the same directory as your video.




