#!/bin/bash
#Simple script to perform the following steps:
# 1. Count the number of bags in the directory
# 2. Create a new directory with the corresponding trial number
# 3. Move the bag to its own directory
# 4. process the emotion recognition video
# 
# Inputs: the name of the directory that contains the rosbags for a given participant
# ----------------------------------------------------------------

#Check that path to block name is supplied
if [ $# -eq 0 ]
  then
    echo "No block supplied"
    exit 1
fi

echo "Absolute path to block $PWD/$1"
blockpath=$PWD/$1

cd $blockpath
nbags=$(ls | wc -l)
echo "Number of rosbags in block directory: $nbags"

#COUNTER=1
#for i in $( ls -1tr | head -$nbags ); do
 #   name=$(echo "trial_$COUNTER")
  #  mkdir $name
   # mv $i $name
    #echo "moving $i into $name"
    #let COUNTER=COUNTER+1
#done
echo "Starting image extraction from bags..."

for i in $( ls -1tr | head -$nbags ); do
# cd into trial directory
    cd $i
    bagname=$(ls -1tr)
# Extract compressed images from rosbag
    echo "extracting $i/$bagname"
    roslaunch wheelchair_navigation extract_images.launch bag_name:=$PWD/$bagname
# Move extracted images to a new directory
    echo "Moving images..."
    mkdir images
    mv ~/.ros/frame* images/
    echo "Done moving images"
# Generate video from images
    echo "Creating video..."
    mencoder "mf://images/*.jpg" -mf type=jpg:fps=20 -o output.avi -speed 1 -ofps 20 -ovc lavc -lavcopts vcodec=mpeg4:mbd=2:trell -oac copy -o output.avi
    echo "Done creating video"
# Run emotion detector
$HOME/cpp-sdk-samples/build/video-demo/video-demo --input output.avi --data $HOME/affdex-sdk/data --faceMode 0
# cd back into block directory
    cd ../
done

