#!/bin/bash
#Simple script to perform the following steps:
# 1. extract compressed images from rosbag
# 2. move the extracted frames to a subdirectory called images
# 3. produce a video with the extracted frames
# 4. run the emotion detector on the video
# 
# Inputs: the rosbag path
# ----------------------------------------------------------------

#Check that path to bag is supplied 
if [ $# -eq 0 ]
  then
    echo "No bag supplied"
    exit 1
fi


echo "Absolute path to bag $PWD/$1"
bagpath=$PWD/$1

# Extract compressed images from rosbag
echo "Press the space bar to begin extracting images..."
roslaunch wheelchair_navigation extract_images.launch bag_name:=$bagpath
echo "Done extracting images"

# create images directory
echo "Moving images..."
mkdir images

# move images newly created directory
mv ~/.ros/frame* images/
echo "Done moving images"

# create video
echo "Creating video..."
mencoder "mf://images/*.jpg" -mf type=jpg:fps=20 -o output.avi -speed 1 -ofps 20 -ovc lavc -lavcopts vcodec=mpeg4:mbd=2:trell -oac copy -o output.avi
echo "Done creating video"
#run emotion detector
$HOME/cpp-sdk-samples/build/video-demo/video-demo --input output.avi --data $HOME/affdex-sdk/data --faceMode 0
