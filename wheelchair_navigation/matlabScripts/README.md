Directory containing matlab scripts to be used with the wheelchair. 

Useful scripts:
-----------------
- demo_scripts/test_connection.m : 

		Use it to simply drive the wheelchair using the omni joystick.
***	

- demo_scripts/testEband_planner.m

  		Use it to tune elastic band planner parameters. 
***	

- SC_wCallbacks.m

  		Main script to run the shared control policies. This scripts calls joySharedCallbackFcn.m
***