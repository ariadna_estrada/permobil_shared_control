%% Shared control and Obstacle Avoidance using Powered WheelChair
% Introduction
% This example demonstrates different shared control policies on a velocity
% controlled differencial drive robot. The example implements the VFH+
% algorithm for obstacle avoidance in certain policies.  
 
%% Connect to the ROS Network
close all;
rosshutdown;
rosinit('localhost');

%% Load files 
% Occupancy grid (map), goal locations, sounds, and blending modes.
load('lab_20180822.mat');
load('lab_goals.mat');
load('sounds.mat');
load('intervention_modes.mat');

%Load (if exist) predetermined start and end locations for experiment
n = size(modes,2);
for i = 1:n    
    str = modes(i);
    var_name = char(strcat(str,'_config'));
    if exist(strcat(var_name, '.mat'), 'file') == 2
        disp({'Loading: ', var_name});
        file=load(var_name);
        handles.goal_config{i,1} = file.(var_name);wsStates
    end
end

%% Create ROS node to check for collisions
collisionChecker = robotics.ros.Node('collisionChecker', 'http://localhost:11311');
subfootprint = robotics.ros.Subscriber(collisionChecker,'/move_base/local_costmap/footprint', @footprintCallbackFcn);
subcostmap= robotics.ros.Subscriber(collisionChecker, 'move_base/local_costmap/costmap', @localCostmapCallbackFcn);

%% Create publishers and subscribers and make them part of a struct
% (|handles|) which you pass into the joystick callback when it is created. 

%Set up publishers
[handles.Vel.pub, handles.Vel.msg] = rospublisher('/cmd_vel', 'JoyControl/joystick');
[handles.actionClient, handles.msgGoal] = rosactionclient('/move_base');
handles.msgGoal.TargetPose.Header.FrameId = 'map';
handles.actionClient.FeedbackFcn = [];

% Set up subscribers with callbacks
sublaser = rossubscriber('/scan_multi', @laserCallbackFcn);
subamcl = rossubscriber('/amcl_pose', @amclCallbackFcn);
subeband = rossubscriber('/eband_cmd_vel', @ebandCallbackFcn);

% Set up subscribers without callbacks
handles.subpath = rossubscriber('/move_base/EBandPlannerROS/global_plan');
handles.substatus = rossubscriber('/move_base/status');

%% Add files/variables to the handles struct
handles.map = map;
handles.world_goals = lab_goals;
handles.sounds = sounds;
handles.modes = modes;
handles.collisionChecker = collisionChecker;

%% Create VFH object
vfh = VectorFieldHistogram('RobotRadius', 0.3, 'SafetyDistance', 0.2, 'MinTurningRadius', 0.2, 'HistogramThresholds', [60 100], 'DistanceLimits', [0.1, 3]);
vfh.PreviousDirectionWeight = 10;
vfh.TargetDirectionWeight = 9;
vfh.CurrentDirectionWeight = 2;
vfh.NarrowOpeningThreshold = 0.8;

handles.vfh = vfh;

%% Subscribe to joystick from user and call main shared control function
%clear persistent variables from pervious runs 
clear joySharedCallbackFcn;
subjoy = rossubscriber('/joy_user', {@joySharedCallbackFcn, handles});


        file=load(var_name);
        handles.goal_config{i,1} = file.(var_name);wsStates
    end
end

%% Create ROS node to check for collisions
collisionChecker = robotics.ros.Node('collisionChecker', 'http://localhost:11311');
subfootprint = robotics.ros.Subscriber(collisionChecker,'/move_base/local_costmap/footprint', @footprintCallbackFcn);
subcostmap= robotics.ros.Subscriber(collisionChecker, 'move_base/local_costmap/costmap', @localCostmapCallbackFcn);

%% Create publishers and subscribers and make them part of a struct
% (|handles|) which you pass into the joystick callback when it is created. 

%Set up publishers
[handles.Vel.pub, handles.Vel.msg] = rospublisher('/cmd_vel', 'JoyControl/joystick');
[handles.actionClient, handles.msgGoal] = rosactionclient('/move_base');
handles.msgGoal.TargetPose.Header.FrameId = 'map';
handles.actionClient.FeedbackFcn = [];

% Set up subscribers with callbacks
sublaser = rossubscriber('/scan_multi', @laserCallbackFcn);
subamcl = rossubscriber('/amcl_pose', @amclCallbackFcn);
subeband = rossubscriber('/eband_cmd_vel', @ebandCallbackFcn);

% Set up subscribers without callbacks
handles.subpath = rossubscriber('/move_base/EBandPlannerROS/global_plan');
handles.substatus = rossubscriber('/move_base/status');

%% Add files/variables to the handles struct
handles.map = map;
handles.world_goals = lab_goals;
handles.sounds = sounds;
handles.modes = modes;
handles.collisionChecker = collisionChecker;

%% Create VFH object
vfh = VectorFieldHistogram('RobotRadius', 0.3, 'SafetyDistance', 0.2, 'MinTurningRadius', 0.2, 'HistogramThresholds', [60 100], 'DistanceLimits', [0.1, 3]);
vfh.PreviousDirectionWeight = 10;
vfh.TargetDirectionWeight = 9;
vfh.CurrentDirectionWeight = 2;
vfh.NarrowOpeningThreshold = 0.8;

handles.vfh = vfh;

%% Subscribe to joystick from user and call main shared control function
%clear persistent variables from pervious runs 
clear joySharedCallbackFcn;
subjoy = rossubscriber('/joy_user', {@joySharedCallbackFcn, handles});

