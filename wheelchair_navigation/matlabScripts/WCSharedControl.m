%% Shared control and Obstacle Avoidance using Powered WheelChair
%% Introduction
% This example demonstrates several shared control policies on a velocity
% controlled differencial drive robot. The example implements the VFH+
% algorithm for obstacle avoidance. 
 
%% Connect to the WheelChair
close all;
rosshutdown;
rosinit('localhost');

% To be updated
load('lab_209.mat');
load('lab209_global_costmap');
load('lab_goals.mat');
load ('splat.mat');

global costmap
global footprint
global pose_data
global u_joy
global laserGlobal

%% Initialize the Obstacle Avoidance Algorithm
% * Generate a struct that contains the gains used in the VFH+ algorithm. 
  gains.goalTargeting = 90;          % Gain for desire to reach goal
  gains.forwardPath = 10;            % Gain for moving forward 
  gains.continuousPath = 5;         % Gain for maintaining continuous path
  gains.obstacleAvoid = 30;        % Gain for avoiding obstacles
%%
% * Create publishers and subscribers and make them part of a struct
% (|timerHandles|) which you pass into the timer when it is created. 

%Set up publishers
  timerHandles.pub = rospublisher('/cmd_vel'); 
  timerHandles.pubmsg = rosmessage('JoyControl/joystick');
  
  timerHandles.pubGoal = rospublisher('/move_base/goal');
  timerHandles.msgGoal = rosmessage('move_base_msgs/MoveBaseActionGoal');
  timerHandles.msgGoal.Goal.TargetPose.Header.FrameId = 'map';
  
% Set up subscribers
  timerHandles.substatus = rossubscriber('/move_base/status');
  timerHandles.sublaser = rossubscriber('/scan_multi');  
  timerHandles.subamcl = rossubscriber('/amcl_pose');
  timerHandles.subjoy = rossubscriber('/joy_user'); 
  timerHandles.subeband = rossubscriber('/eband_cmd_vel');
  timerHandles.subpath = rossubscriber('/move_base/EBandPlannerROS/global_plan');
  timerHandles.subcostmap_local = rossubscriber('/move_base/local_costmap/costmap',{@occupancyGrid_from_costmap_callback});
  timerHandles.subcostmap_global = rossubscriber('/move_base/global_costmap/costmap');
  timerHandles.subfootprint = rossubscriber('/move_base/global_costmap/footprint', {@footprintCallbackFcn});
%%
% * Add the gains to the |timerHandles|
% struct:
%%
  timerHandles.gains = gains;
  timerHandles.map = map;
  timerHandles.global_costmap = global_costmap;
  timerHandles.world_goals = lab_goals;
  timerHandles.goal_sound = y;
  timerHandles.modes = {'Teleop','DisagreementBlend', 'DynamicSC',...
                        'EfficiencyBlend','CollisionAvoidance', ...
                        'CollisionFree', 'NearGoalAutoSwitch','HighLevelBlend', ...
                        'AwayFromGoalBlend', 'TestStopCollision'};

%% create VFH object
  vfh = VectorFieldHistogram('RobotRadius', 0.4, 'SafetyDistance', 0.4, 'MinTurningRadius', 0.1, 'HistogramThresholds', [20 80], 'DistanceLimits', [0.1, 2]);
  vfh.PreviousDirectionWeight = 10;
  timerHandles.vfh = vfh;
%% Create timer
  timer1 = timer('TimerFcn',{@WCSharedControlTimer,timerHandles},'Period',0.01,'ExecutionMode','fixedSpacing');
  timer1.StopFcn = {@stopCallback};
  start(timer1);
%%  Visualize VFH
% If you need to tune the VFH parameters, it might be useful to visualize
% what is happening. Run the following lines to show the vfh object
% results.
% 
%   while strcmp(timer1.Running, 'on')
%     segbotShowVFH(vfh);
% % %     pause(1);    
%   end
%% Stopping the timer
% * To stop the timer while in the middle of a loop, close the
% figure window. If the timer does not clear, a safe way to clear all
% timers is to use the following command:
%%
%   delete(timerfindall)

%% References
% [1] I. Ulrich, J. Borenstein, "VFH+: reliable obstacle avoidance for fast mobile robots," 
% In Proceedings of IEEE(R) International Conference on Robotics and Automation (ICRA), 1998, vol. 2, 
% pp. 1572-1577
% displayEndOfDemoMessage(mfilename)