function amclCallbackFcn(~, message)
%amclCallbackFcn- Process ROS message with localization data
global poseData

if isempty(message)
    poseData = [0 0 0];
    return;
end

pos = message.Pose.Pose;
xpos = pos.Position.X;
ypos = pos.Position.Y;
quat = pos.Orientation;
angles = quat2eul([quat.W quat.X quat.Y quat.Z]);
theta = angles(1);
pose_data = [xpos, ypos, theta];
poseData = pose_data;
end

