function ebandCallbackFcn(~, ebandMsg)
%ebandcallbackFcn receives twist type message produced by autonomous
%planner and constructs vector with the linear and angular components of
%the control. 
%u_eband = [linear_velocity, angular_velocity]; 
%Ariadna Estrada May/30/2018

global u_eband

u(1) = ebandMsg.Linear.X;
u(2) = ebandMsg.Angular.Z;

u_eband = u;
end