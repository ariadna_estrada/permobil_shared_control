function ebandTestCallbackFcn(~, message, cmd_vel)
%ebandCallbackFcn Takes twist message produced by eband planner and sends
%it to the wheelchair using a JoyControl/joystick message type

cmd_vel.msg.Linear = message.Linear.X;
cmd_vel.msg.Angular = message.Angular.Z;

send(cmd_vel.pub, cmd_vel.msg);
end

