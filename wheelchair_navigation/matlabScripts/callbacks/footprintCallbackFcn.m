function footprintCallbackFcn(~,message)
% footprintCallback(message) Receives a polygon type message published by the local
% costmap and constructs an n x 3 array with the location and orientation of the robot's
% footprint (in this case a Powered Wheelchair). Following, the function will perform a
% prediction of the robot pose given an input u_blend and evaluate the probability of
% landing on an occupied cell. The function shares information with the rest of the matlab
% node through global variables.

global poseData
global costmap
global u_blend
global in_collision
global near_collision
global footprintData

if isempty(poseData)
    poseData = [0,0,0];
end

%Still needs tuning, this current config was only tested for Collision Free
collisionThresh = 0.7; %Min occupancy value to be considered in collision
u_time = 2; %Time for which u_blend will be applied for the motion prediction model

%Extract footprint data from ROS message
nPoints = numel(message.Polygon.Points);
footprint = zeros(nPoints,3);
for i=1:nPoints
   footprint(i,1) = message.Polygon.Points(i,1).X;
   footprint(i,2) = message.Polygon.Points(i,1).Y;
end

footprint(:,3) = poseData(3); %Angle of points is the same for all. Using amcl's estimated heading.
footprintData = footprint; %Global variable to share info with other nodes

%Separating footrpint points from the front and back of the PWC. We only run the motion
%prediction model for the points we're interested depending on the sign of the velocity
%command. 
legs_footprint = footprint(3:4,:);
back_footrpint = footprint(7:8,:);

%Run forward model and evaluate occupancy in the predicted next pose
if ~isempty(u_blend)
    %Predit the location of the robot if the input u_blend was applied for u_time.
    
    %If moving forward, perform pose prediction only for the front of the PWC
    if(u_blend(1)>=0)
        next_pose = sample_motion_nonholonomic(u_blend, legs_footprint, u_time);
    else
    %If moving backwards, perform pose prediction only for the back of the PWC
        next_pose = sample_motion_nonholonomic(u_blend, back_footrpint, u_time);
    end
    
    try
        prob_collision = getOccupancy(costmap, next_pose(:,1:2));
    catch me
        %warning(me.message);
        in_collision = 0; 
        near_collision = 0;
        return
    end
    
    %Set collision prediction to true if any of the footprint points lands on a cell with
    %an occupancy value higher than a threshold.
    near_collision = any(prob_collision > (collisionThresh*0.5));
    in_collision = any(prob_collision > collisionThresh);
else
    in_collision = 0;
end

%Filter in_collision with last 5 samples to make it smoother. 
in_collision = filteredCollision(in_collision);

end

function in_collision = filteredCollision(class)
%filteredCollision(class) Keeps the last classification resutls of the collision
%checker. Returns the mode of the last 5 samples. 
persistent collisionHistory

maxDataHistory = 5; %Retain only the last 5 samples

collisionHistory = [collisionHistory; class];
if length(collisionHistory) > maxDataHistory
    collisionHistory(1) = [];
end

in_collision = mode(collisionHistory);

end