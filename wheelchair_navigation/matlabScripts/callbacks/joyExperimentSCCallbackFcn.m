function joyExperimentSCCallbackFcn(~, message, handles)
%joyExperimentSCCallbackFcn Triggered when a joystick message is received.
%   The controller produces motion commands according to different blending policies.

%Declare persistent variables
persistent user_goal    %User's goal
persistent plan_goal    %Planner goal
persistent task_goal    %Each task has two goals (one in each room) task_goal takes values 1 or 2 (char type)
persistent intervention %Intervention mode

%Global variables
global poseData  %Published by amclCallbackFcn
global u_eband   %Published by ebandCallbackFcn

%Constants
goalThresh = 0.75; %distance in meters to be considered inside the goal region

if (handles.begin_tb.Value)
    %Set experiment conditions
    if isempty(intervention)
        intervention = handles.current_mode_label.String;
        user_goal = [ str2double(handles.user_x.String), str2double(handles.user_y.String) ];
        plan_goal = [ str2double(handles.planner_x.String), str2double(handles.planner_y.String) ];
        task_goal = handles.goal12_label.String;
        setExperimentGoal(plan_goal, handles);
    end
    
    
    %Execute the main control loop
    try
        %Get user command from joystick
        u_joy = getJoystickData(message);
        
        %Store the current planner's command
        u_robot = u_eband;
        
        %Check planner status
        status = handles.actionClient.GoalState;
        if ~( strcmp(status, 'active') || strcmp(status, 'pending') || strcmp(status, 'preempted') || strcmp(status, 'succeeded'))
            disp('resending goal');
            setExperimentGoal(plan_goal, handles);
        end
        %Computed shared control command. Three cases:
        % 1) The user has reached the goal.
        % 2) The user is issuing non-zero velocity commands.
        % 3) The user is issuing zeroed velocity commands.
        if ( norm( user_goal - poseData(1:2), 2 ) < goalThresh || strcmp(status, 'succeeded') )
            disp('Goal Reached');
            intervention = [];
            if strcmp(task_goal, '1')
                %First sub-goal completed. User can keep driving, prepare to call new
                %goal.
                u_shared = u_joy; user_weight = 1;
                set(handles.goal12_label, 'String', '2');
                %Play single bell sound
                sound(handles.sounds.goal_sound.data(1:42700, :),handles.sounds.goal_sound.fs);
            else
                %Second sub-goal completed. Trial completed.
                u_shared = [0,0]; user_weight = 1;
                set(handles.begin_tb, 'Value', 0);
                set(handles.goal12_label, 'String', '1');
                %Play double bell sound
                sound(handles.sounds.goal_sound.data(43000:end, :),handles.sounds.goal_sound.fs);
            end
            %Call begin/complete toggle button callback to update goals.
            run_experiment('begin_tb_Callback', handles.begin_tb, [], guidata(handles.output));
            
        elseif( norm( u_joy ) > 0 )
            [u_shared, user_weight] = wheelchairController(handles, u_joy, u_robot, poseData, user_goal, intervention);
            
        else
            u_shared = [0,0]; user_weight = 1;
            
        end
        
        % store current time
        current_time = rostime('now', 'system');
        
        %Publish linear and angular velocities to wheelchair
        handles.Vel.msg.Linear  = u_shared(1);
        handles.Vel.msg.Angular = u_shared(2);
        handles.Vel.msg.T = current_time;
        send(handles.Vel.pub,handles.Vel.msg);
        
        %Publish user weight
        handles.weight.msg.Data = user_weight;
        send(handles.weight.pub, handles.weight.msg);
        
        %Re-publish user command used for the blend
        handles.u_joy.msg.Twist.Linear.X = u_joy(1);
        handles.u_joy.msg.Twist.Angular.Z = u_joy(2);
        handles.u_joy.msg.Header.Stamp = current_time;
        send(handles.u_joy.pub, handles.u_joy.msg);
        
        %Re-publish planner command used for the blend
        handles.u_robot.msg.Twist.Linear.X = u_robot(1);
        handles.u_robot.msg.Twist.Angular.Z = u_robot(2);
        handles.u_robot.msg.Header.Stamp = current_time;
        send(handles.u_robot.pub, handles.u_robot.msg);
        
    catch me
        fprintf(2, 'There was an error in the main control loop! The message was:\n%s\n', me.message);
        intervention = [];
    end
else
    intervention = [];
end

%% Callback functions and Nested Functions
    function u_joy = getJoystickData(joyMsg)
        % getJoystickData - Retreive user's desired steering direction from joystick
        % message. Check whether the signal is above the minimum speed to produce
        % movement.
        
        % Deadzone
        minSpeed = 0.15;
        % Max velocities
        maxAngular = 0.75;
        maxLinear = 1;
        
        linear = double(joyMsg.Axes(2));
        angular = double(joyMsg.Axes(1));
        speed = norm([linear, angular]);
        
        % Filter out input values below minSpeed to avoid "clicking" sound
        % from the motors. Low speeds will engage the motors but will be
        % unable to produce any movement.
        if(speed < minSpeed)
            linear = 0; angular = 0;
        end
        
        u_joy = [linear*maxLinear, angular*maxAngular];
    end

    function setExperimentGoal(plan_goal, handles)
        %setExperimentGoal - Sends goal to planner
        handles.msgGoal.TargetPose.Pose.Position.X = plan_goal(1);
        handles.msgGoal.TargetPose.Pose.Position.Y = plan_goal(2);
        handles.msgGoal.TargetPose.Pose.Orientation.W = 1;
        handles.msgGoal.TargetPose.Header.Stamp = rostime('now');
        sendGoal(handles.actionClient, handles.msgGoal);
        
        %publish a message with the intervention being used.
        handles.intervention.msg.Data = intervention;
        send(handles.intervention.pub, handles.intervention.msg);
    end
end

% Main control loop
function [u_shared, user_weight] = wheelchairController(handles, u_joy, u_eband, poseData, goal, intervention)
%wheelchairController Returns u_shared consisting of linear and angular velocity commands
%to publish to the robot. Also returns the user_weight used for the given blending policy.

persistent prevGoal;    %Keeps track of goal. Used to clear object from WCData class when it changes.
persistent dataobj;     %Object from WCData class. Used by disagreement blend and efficiency blend.
persistent prevW;       %Keeps track of previous angular velocity sent to the PWC. Used by DynamicSC.
persistent wcState;     %Object from wcStates class. Used by Woz_policy to keep track of the hysterecic principle. 

global costmap          %Published by localCostmapCallbackFcn
global laserData        %Published by laserCallbackFcn

switch intervention
    case {'Efficiency_A', 'Efficiency_B'}
        %Blending based on approach described in "An adaptive scheme for Wheelchair
        %Navigation Collaborative Control" by Urdiales
        
        %Compute next partial goal using eband planned trajectory.
        testPoint = get_test_point(handles.subpath.LatestMessage);
        
        %Efficiency constants (values approx to those paper)
        c_sf = 4.5;     %smoothness
        c_tl = 4.5;   %directiveness
        c_sc = 4.5;   %safety
        
        %Compute vector angle from robot motion command (Fig 1)
        alpha_diff_r = atan2(u_eband(2), u_eband(1));
        
        %Compute vector angle from human motion command (Fig 1)
        alpha_diff_h = atan2(u_joy(2), u_joy(1));
        
        %Common for robot and human
        alpha_dest = simpleComputeTargetAngle(testPoint, poseData);
        alpha_min = laserData.minAngle;
        
        %Compute robot/planner efficiencies
        n_smooth_r = exp(-c_sf * abs( alpha_diff_r ));
        n_direct_r = exp(-c_tl * abs( angdiff(alpha_dest, alpha_diff_r) ));
        n_safety_r = 1 - exp(-c_sc * abs( angdiff(alpha_min, alpha_diff_r) ));
        
        n_robot = mean([n_smooth_r, n_direct_r, n_safety_r]);
        
        %Compute human efficiencies
        n_smooth_h = exp(-c_sf * abs( alpha_diff_h ));
        n_direct_h = exp(-c_tl * abs( angdiff(alpha_dest, alpha_diff_h) ));
        n_safety_h = 1 - exp(-c_sc * abs( angdiff(alpha_min, alpha_diff_h) ));
        
        n_human = mean([n_smooth_h, n_direct_h, n_safety_h]);
        
        %print for debugging
%         fprintf('r_sm: %.2f\t r_di: %.2f\t r_sa: %.2f\t r_eff: %.2f\n', n_smooth_r, n_direct_r, n_safety_r, n_robot);
%         fprintf('h_sm: %.2f\t h_di: %.2f\t h_sa: %.2f\t h_eff: %.2f\n', n_smooth_h, n_direct_h, n_safety_h, n_human);
        
        %Human and planner contributions are computed according to  "Efficiency based
        %modulation for wc driving collaborative control". (2010) From same research
        %group.
        
        %Compute modulation parameter K according to section III.
        if ( (n_human >= 0.85) || (n_human > (1.5 * n_robot)) )
            %user is doing well or at least, clearly outperforming the machine
            K = 0.75;
        elseif ( (n_human < 0.75) && ( (0.5 * n_robot) < n_human ) && ( n_human < (1.5 * n_robot) ) )
            K = 0.5;
        else
            K = 0.25;
        end
        
        %one approach to compute shared control: normalize weights then blend
        alpha = (K * n_human) / (K * n_human + (1-K) * n_robot); %normalize weights
        u_shared = alpha * u_joy + (1 - alpha) * u_eband;
        
        user_weight = alpha;  
        
    case {'Dynamic_A', 'Dynamic_B'}
        %Blending approach based on Dynamic Shared Control by Qinan Li et al.
        %Optimizes over safety, comfort, and obedience.
        %Initialize previous angular velocity
        if isempty(prevW)
            prevW = 0;
        end

        %Reduce linear speed if nearest obstacle within 100 deg of the front of the robot
        %is less than obsThresh
        
        obsThresh = 0.75; %Distance (m) at which obstacles will cause the final speed to be reduced
        predictTime = 2.5; %Time (s) that the kinematic model will use for pose prediction
        
        %Optimization parameters constants. See paper for details on how to
        %chose parameters.
        alpha = 4.5;
        beta  = 4.5;    %comfort constant st. comfort = 0 when |w - w_o| = 1.5
        gamma = 4.5; %obedience constant st. obedience = 0 when |xi - xi_star| = pi/2
        
        joy_ang = atan2(u_joy(2), u_joy(1));
        v = u_joy(1);
    
        %Initialize optimization variable
        prev_best = 0;
        best_k1 = 1;
        best_set = [];
        %Iterate over weights starting from full control to user to full control to
        %planner.
        stepK = -0.2; %Step size to decrement user weight
        for k1 = 1:stepK:0
            k2 = 1-k1;
            w = k1*u_joy(2) + k2*u_eband(2);
            u_ang = atan2(w, v);
            
            next_pose = sample_motion_nonholonomic([v,w], poseData, predictTime);
            try
                cost = getOccupancy(costmap, next_pose(1:2));
            catch me %#ok<NASGU>
                warning('costmap not valid');
                cost = 0.8;
            end

            safety = exp(-alpha * abs(cost));
            comfort = exp(-beta * abs(w - prevW));
            obedience = exp(-gamma * abs( angdiff ( u_ang , joy_ang)));
            
            current_set = [safety, comfort, obedience];
            current_min = min(current_set);
            
            if current_min > prev_best
                prev_best = current_min;
                w_blend = w;
                best_k1 = k1;
                best_set = current_set;
            end
        end
        %print best set for debugging
%         fprintf('user: %.1f bestSafety: %.3f bestComfort: %.3f bestObedience: %.3f \n',best_k1, best_set(1), best_set(2), best_set(3));

        if(laserData.mindist < obsThresh && v > 0)
            v = u_eband(1);
        end
        
        u_shared = [v, w_blend];
        user_weight = best_k1;
        prevW = w_blend;
        
        
    case {'HighLevel_A', 'HighLevel_B'}
        %Blending with High Level Goal from Erdogan paper Steps away control from the
        %user based on euclidean distance to the goal. Perhaps should be plan
        %distance.
        dc = 8; %At what distance the control is at 50/50
        tau = 0.75;  %How fast the control steps away from the user, higher values produce steeper changes.
        
        d = norm(goal-poseData(1:2),2); %euclidean distance to goal
        alpha = 1 / (1 + exp(-tau*(d - dc))); %user weight
        deltaAlpha = min(alpha/5,0.1); %Step to decrement user weight in case of collision
        
        u_shared =  alpha * u_joy + (1 - alpha) * u_eband;
        
        while (checkCollision(u_shared, 2, 0.3) && alpha > 0)
            if alpha > deltaAlpha
                alpha = alpha - deltaAlpha;
            else
                alpha = 0;
            end
            u_shared =  (alpha * u_joy + (1 - alpha) * u_eband );
        end
        user_weight = alpha;
%         fprintf('user weight: %.2f\n', user_weight);
        
    case {'Simple_A', 'Simple_B'}
        %Naive linear blending approach always giving same amount of control to user
        %and planner.
        
        alpha = 0.5;
        deltaAlpha = 0.1; %Step to decrement user's weight in case of collision
        
        u_shared = alpha * u_joy + (1 - alpha) * u_eband;
        
        while (checkCollision(u_shared, 2, 0.3) && alpha > 0)
            if alpha > 0.1
                alpha = alpha - deltaAlpha;
            else
                alpha = 0;
            end
            u_shared =  (alpha * u_joy + (1 - alpha) * u_eband );
        end
        user_weight = alpha;
        
    case {'Disagreement_A', 'Disagreement_B'}
        %Blend according to a disagreement metric between the user and the planner inputs.
        %If user is consistent with the planner (maintains an average angular difference
        %below treshold angTresh) then the planner starts taking over. It will take a high
        %angular difference (maxDiff) between the user and the planner for the user to be
        %granted complete control again. For now the blending only modifies the angular
        %component of the velocity, keeping the user in full control of the linear
        %component at all times.
        
        %Initialize WCData object
        if isempty(dataobj)
            dataobj = WCData();
            prevGoal = goal;
        end
        
        if (goal ~= prevGoal)
            dataobj.clearData;
        end

        minSamples =  30; %User will always be in control if number of angular difference samples is below this parameter.
        maxDiff = pi/2;   %If the angular difference between the planner and the user is higher than this parameter, the user will re-gain complete control.
        angTresh = pi/3;  %Minimum average angular difference to be maintained by the user to be considered 'in agreement' with the planner.
        tau = 0.66;       %weight constant
        ebandAngle = atan2(u_eband(2), u_eband(1));
        joy_ang = atan2(u_joy(2), u_joy(1));
        
        cDif = abs(angdiff(joy_ang,ebandAngle)); %Current angular difference between user and planner
        dataobj.addAngle( cDif );   %Add current angular difference to WCData object
%         fprintf('cDif = %.2f \t aDif = %.2f \t stdD = %.2f\n',cDif, dataobj.averageValue, dataobj.stdDevValue);
        
        if (length(dataobj.DataHistory) < minSamples || dataobj.averageValue > angTresh)
            %follow user command if disagreement is high or number of samples is too low.
            disp('user in control');
            u_shared = u_joy;
            alpha=1;
        else
            %User seems to be agreeing with the planner, but direction changes drastically over the
            %last 30 samples (1.5 seconds)
            if(mean(dataobj.DataHistory(end-29:end)) > maxDiff)
                disp('user back in control');
                dataobj.clearData();
                u_shared = u_joy;
                alpha=1;
            else %User is overall agreeing with planner so planner starts taking over control
%                 alpha = max(0,1 - exp(1.5*(0.2 - dataobj.averageValue)));
%                 alpha = 1 - exp(-tau * abs(dataobj.averageValue));
%               alpha = abs(dataobj.averageValue) * angTresh;
                alpha = exp(tau*abs(dataobj.averageValue)) - 1;
                u_shared(1) = u_joy(1);
                u_shared(2) =  (alpha * u_joy(2) + (1 - alpha) * u_eband(2));               
            end
        end
        
        %Decrement alpha iteratevly to avoid obstacles.
        while (checkCollision(u_shared, 2, 0.3) && alpha > 0)
            if alpha > 0.1
                alpha = alpha - 0.1;
            else
                alpha = 0;
            end
            u_shared =  (alpha * u_joy + (1 - alpha) * u_eband );
        end
        user_weight = alpha;
%         fprintf('user weight: %.2f\n', user_weight);
        
    case {'CollisionAvoidance_A', 'CollisionAvoidance_B'}
        %Intervene only when user is going to collide
        alpha = 1; %Initial user weight
        deltaAlpha = 0.2; %Step to decrement user's weight in case of collision
        
        u_shared = alpha * u_joy + (1 - alpha) * u_eband;
        
        while (checkCollision(u_shared, 2, 0.3) && alpha > 0)
            if alpha > 0.1
                alpha = alpha - deltaAlpha;
            else
                alpha = 0;
            end
            u_shared =  alpha * u_joy + (1 - alpha) * u_eband;
        end
        user_weight = alpha;
%         fprintf('user weight: %.2f\n', user_weight);
    
    case {'AwayFromGoal_A', 'AwayFromGoal_B'}
        %Blending similar to HighLevelBlend but inverse values for alpha. When the
        %user is away from the goal, the planner will have a higher weight. As the
        %user gets closer to the goal it gets more control.
        
        dc = 10; %At what distance the control is at 50/50
        tau = 0.75;  %How fast the control steps away from the planner, higher values produce steeper changes.
        
        d = norm(goal-poseData(1:2),2);
        alpha = 1 - (1 / (1 + exp(-tau*(d - dc)))); %user weight
        deltaAlpha = min(alpha/5,0.1); %Step to decrement user weight in case of collision
        
        u_shared =  (alpha * u_joy + (1-alpha) * u_eband );
        
        while (checkCollision(u_shared, 2, 0.3) && alpha > 0)
            if alpha > deltaAlpha
                alpha = alpha - deltaAlpha;
            else
                alpha = 0;
            end
            u_shared =  (alpha * u_joy + (1 - alpha) * u_eband );
        end
        user_weight = alpha;
%         fprintf('user weight: %.2f\n', user_weight);
    case{'Woz_A', 'Woz_B'}
        %Policy 2 described for the wizard of oz study. (steering
        %correction)
        %Initialize wcState object
        if isempty(wcState)
            wcState = wcStates.a1; %user starts with alpha = 1 (WoZ policy)
        end

        d0 = 1.0; %distance (m) from obstacle at which the user loses control
        d1 = 1.5; %distance (m) from obstacle at which the user re-gains control
        
        % Toggle between states if needed
        if (laserData.mindist < d0)
            wcState = wcStates.a0; 
        elseif (laserData.mindist > d1)
            wcState = wcStates.a1;
        end
        
        if (wcState == wcStates.a0)
            u_shared = [min(u_joy(1), u_eband(1)), u_eband(2)];
            alpha = 0;
        elseif(wcState == wcStates.a1)
            u_shared = u_joy;
            alpha = 1;
        end

        user_weight = alpha;
%         fprintf('user weight: %.2f\n', user_weight);
end

prevGoal = goal;
end


