function joySharedCallbackFcn(~, message, handles)
    %joySharedCallbackFcn Triggered when a joystick input is received.
    %   The controller produces motion commands according to different
    %   blending policies. A figure displays the map showing the position
    %   of the robot over time. In the same figure, the user can select
    %   between the available shared control policies. Check the "different
    %   goals" checkbox, if the user and the autonomous planner will have
    %   different goals. Select t1 or t2 for different trials during
    %   experiment.
    %
  
    % Declare persistent variables
    persistent goal;            %User's goal 
    persistent plotobj;         %GUI with map and trial options
    persistent mode_popup;      %Popup menu with list of intervention modes
    persistent trial_popup;     %Popup menu to select trial number
    persistent diff_goals_box;  %Checkbox indicating whether the planner gets a different goal
    
    %Global variables
    global poseData  %Published by amclCallbackFcn
    global laserData %Published by laserCallbackFcn
    
    %Constants
    goalThresh = 0.75; %distance in meters to be considered in the goal region
    
    %Create object to visualize data and select controller options.
    if isempty(plotobj)
        goal = [];
    end
    
    if isempty(goal)
        try
            if isempty(plotobj)
                plotobj = WCVisualizer([-10,10,-10,10], handles.map);
                plotobj.FigureHandle.DeleteFcn = {@stopCallback, handles.collisionChecker};
                mode_popup = uicontrol('Style', 'popup', 'String', handles.modes,'Value',1,...
                    'Position', [20 20 150 20], 'Parent', plotobj.FigureHandle,...
                    'Callback', {@clearGoal});
                trial_popup = uicontrol('Style', 'popup', 'String', [{'t1'}, {'t2'}],'Value',1,...
                    'Position', [600 20 150 20], 'Parent', plotobj.FigureHandle,...
                    'Callback', {@clearGoal});
                diff_goals_box = uicontrol('style','checkbox','units','pixels', ...
                    'position',[200,20,200,15],'string','Different Goals',...
                    'Parent', plotobj.FigureHandle,...
                    'Callback', {@clearGoal});
            end
        catch me
            fprintf(2, 'There was an error creating the plot object! The message was:\n%s\n', me.message);
            plotobj = [];
        end
               
        %Obtain goal by sampling a set of pre-selected goals If pre-sets don't exist,
        %generate random goal
        goal = setExperimentGoal(handles);
        
    end
    
    % Plot the robot position and orientation every loop
    try
        plotPose(plotobj,poseData);
    catch me
        fprintf(2, 'There was an error plotting the data! The message was:\n%s\n', me.message);
        plotobj = [];
    end
    
    % Execute the main control loop
    try 
        %Get user command from joystick 
        u_joy = getJoystickData(message);

        if norm( goal - poseData(1:2), 2 ) < goalThresh
            disp('Goal Reached');
            u_shared = [0,0]; user_weight = 1;
            goal = [];
            sound(handles.sounds.goal_sound.data,handles.sounds.goal_sound.fs);
        elseif( norm( u_joy ) > 0 )
            [u_shared, user_weight] = wheelchairController(handles, u_joy, laserData, poseData, goal, mode_popup);
        else 
            u_shared = [0,0]; user_weight = 1;
        end

        handles.Vel.msg.Linear  = u_shared(1);
        handles.Vel.msg.Angular = u_shared(2);
        handles.Vel.msg.T = rostime(user_weight); %User weight instead of time.  
        send(handles.Vel.pub,handles.Vel.msg);
        
    catch me
         fprintf(2, 'There was an error in the main control loop! The message was:\n%s\n', me.message);
         plotobj = [];
         goal = [];
    end

    %% Callback functions and Nested Functions  
    function u_joy = getJoystickData(joyMsg)
        % getJoystickData - Retreive user's desired steering direction from joystick
        % message. Check whether the signal is above the minimum speed to produce
        % movement.
        
        % Gains and parameters
        minSpeed = 0.15;
        maxAngular = 0.75;
        maxLinear = 1;
        
        linear = double(joyMsg.Axes(2));
        angular = double(joyMsg.Axes(1));
        speed = norm([linear, angular]);
        
        % Filter out input values below minSpeed to avoid "clicking" sound
        % from the motors. Low speeds will engage the motors but will be
        % unable to produce any movement.
        if(speed < minSpeed)
            linear = 0; angular = 0;
        end
        
        u_joy = [linear*maxLinear, angular*maxAngular];
    end

    function clearGoal(source, ~)
        goal = [];
    end

    function gu = setExperimentGoal(handles)
        %setExperimentGoal - Sets a goal according to the intervention mode and the trial.
        % if predetermined goal does not exist, samples a random goal.
        
        mode = mode_popup.Value;
        trial = trial_popup.String{trial_popup.Value};
        if  isfield(handles,'goal_config{mode}') == 0
            %Assign semi-random goal if there is no predefined value
            gu = selectRandomGoal(handles);
        else
            %Assign goal from pre-sets
            gu = handles.goal_config{mode}.(trial).end;
            plotStart(plotobj, handles.goal_config{mode}.(trial).start)
        end
        
        %Check whether planner gets a different goal
        if diff_goals_box.Value && ~isempty(handles.goal_config{mode})
            gp = handles.goal_config{mode}.(trial).diffGoal;
        else
            gp = gu;
        end
        
        handles.msgGoal.TargetPose.Pose.Position.X = gp(1);
        handles.msgGoal.TargetPose.Pose.Position.Y = gp(2);
        handles.msgGoal.TargetPose.Pose.Orientation.W = 1;
        handles.msgGoal.TargetPose.Header.Stamp = rostime('now');
        sendGoal(handles.actionClient, handles.msgGoal);
        plotGoal(plotobj, gu);
    end

    function gu = selectRandomGoal(handles)
        %selectRandomGoal - Selects a goal from an array ofgu goal points.
        world_goals = handles.world_goals;
        current_pos = poseData(1:2);
        minDistance = 5; %Next goal should be at least minDistance away

        %Set user's random goal
        gu = find_random_far_goal(world_goals, current_pos, minDistance);
    end
end

%% Main control loop
function [u_shared, user_weight] = wheelchairController(handles, u_joy, laser, poseData, goal, popup)
%wheelchairController Returns u_shared consisting of linear and angular velocity commands to
%publish to the robot. Also returns the user_weight used for the given blending
%policy. Some policies use some the collision avoidance node and  will store the
%proposed control in u_blend. The u_blend control will be checked for possible
%collisions. Once the proper collision avoidance action has been computed, the final
%control will be stored in u_shared.
    
      
    persistent prevState;   %Keeps track of blending mode. Used to reset VFH object when blending mode changes.
    persistent prevGoal;    %Keeps track of goal. Used to clear object from WCData class when it changes.
    persistent dataobj;     %Object from WCData class. Used by disagreement blend and efficiency blend.
    persistent prevW;       %Keeps track of previous angular velocity sent to the PWC. Used by DynamicSC.
    persistent wcState;     %Object from wcStates class. Used by Woz_policy to keep track of the hysterecic principle. 

    global u_blend          %Used by footprintCallbackFcn to predict collisions
    global u_eband          %Published by ebandCallbackFcn
    global in_collision     %Published by footprintCallbackFcn
    global near_collision   %Published by footprintCallbackFcn
    global costmap          %Published by localCostmapCallbackFcn

    
    % Time when audio prompt was invoked
    persistent time_called;
    current_time = clock;
    
    % States used for audio prompt
    persistent flag;
    
    if isempty(flag)
        flag = false;
    end
     
    mode = popup.String{popup.Value};
    %Initialize blending mode
    if isempty(prevState)
        prevState = mode;
        disp(mode);
    end
    
    %Initialize WCData object
    if isempty(dataobj)
        dataobj = WCData();
        prevGoal = goal;
    end
    
    %Initialize wcState object
    if isempty(wcState)
        wcState = wcStates.a1; %user starts with alpha = 1
    end

    
    %Initialize previous angular velocity
    if isempty(prevW)
        prevW = 0;
    end
    
    vfh = handles.vfh;
    
    %Print message when operation mode changes and reset vfh  
    if ~strcmp(mode,prevState)
        disp(mode);
        reset(vfh);
    end
    
    if (goal ~= prevGoal)
        dataobj.clearData;
    end
    
    switch mode
        case 'EfficiencyBlend'              
        %Blending based on approach described in "An adaptive scheme for Wheelchair
        %Navigation Collaborative Control" by Urdiales
        
        %Compute next partial goal using eband planned trajectory.
        [ebandPlan.Waypoints, ebandPlan.Dist] = extractWaypoints(handles.subpath.LatestMessage);
        testPoint = get_test_point(ebandPlan.Waypoints);
        
        %Efficiency constants (values from paper for now)
        c_sf = 0.5; %smoothness
        c_tl = 1;   %directiveness
        c_sc = 1;   %safety
        
        %Compute vector angle from robot motion command (Fig 1)
        alpha_diff_r = atan2(u_eband(2), u_eband(1));
        
        %Compute vector angle from human motion command (Fig 1)
        alpha_diff_h = atan2(u_joy(2), u_joy(1));
        
        %Common for robot and human
        alpha_dest = simpleComputeTargetAngle(testPoint, poseData);
        alpha_min = laser.minAngle;
        
        %Compute robot/planner efficiencies
        n_smooth_r = exp(-c_sf * abs( alpha_diff_r ));
        n_direct_r = exp(-c_tl * abs( angdiff(alpha_dest, alpha_diff_r) ));
        n_safety_r = 1 - exp(-c_sc * abs( angdiff(alpha_min, alpha_diff_r) ));
        
        n_robot = mean([n_smooth_r, n_direct_r, n_safety_r]);
        
        %Compute human efficiencies
        n_smooth_h = exp(-c_sf * abs( alpha_diff_h ));
        n_direct_h = exp(-c_tl * abs( angdiff(alpha_dest, alpha_diff_h) ));
        n_safety_h = 1 - exp(-c_sc * abs( angdiff(alpha_min, alpha_diff_h) ));
        
        n_human = mean([n_smooth_h, n_direct_h, n_safety_h]);
        
        %print for debugging
        fprintf('r_sm: %.2f\t r_di: %.2f\t r_sa: %.2f\t r_eff: %.2f\n', n_smooth_r, n_direct_r, n_safety_r, n_robot);
        fprintf('h_sm: %.2f\t h_di: %.2f\t h_sa: %.2f\t h_eff: %.2f\n', n_smooth_h, n_direct_h, n_safety_h, n_human);
        
        %Human and planner contributions are computed according to  "Efficiency based
        %modulation for wc driving collaborative control". (2010) From same research
        %group.
        
        %Compute modulation parameter K according to section III.
        if ( (n_human >= 0.85) || (n_human > (1.5 * n_robot)) )
            %user is doing well or at least, clearly outperforming the machine
            K = 0.75;
        elseif ( (n_human < 0.75) && ( (0.5 * n_robot) < n_human ) && ( n_human < (1.5 * n_robot) ) )
            K = 0.5;
        else
            K = 0.25;
        end
        
        %one approach to compute blend
        alpha = (K * n_human) / (K * n_human + (1-K) * n_robot)
        u_shared = alpha * u_joy + (1 - alpha) * u_eband;
        ratio = norm(u_shared) / norm(u_joy) ;
        while(ratio < 0.8)
        %scale shared command to have at least 80 of the magnitude of u_joy
            u_shared = u_shared *  1.2;
            ratio = norm(u_shared) / norm(u_joy);
        end

        user_weight = K * n_human;        

        case 'DynamicSC'
            %Blending approach based on Dynamic Shared Control by Qinan Li et al.
            %Optimizes over safety, comfort, and obedience.
            
            %Reduce linear speed if nearest obstacle within 100 deg of the front of the robot
            %is less than obsThresh
            
            obsThresh = 0.5; %Distance (m) at which obstacles will cause the final speed to be reduced
            predictTime = 2; %Time (s) that the kinematic model will use for pose prediction
            beta = .09; %comfort constant
            gamma = .09; %obedience constant
            
            joy_ang = atan2(u_joy(2), u_joy(1));
            v = u_joy(1);
            
            %Initialize optimization variable
            prev_best = 0;
            best_k1 = 1;
            
            %Iterate over weights starting from full control to user to full control to
            %planner. 
            stepK = -0.2; %Step size to decrement user weight 
            for k1 = 1:stepK:0
               k2 = 1-k1;
               w = k1*u_joy(2) + k2*u_eband(2);
               u_ang = atan2(w, v);
               
               next_pose = sample_motion_nonholonomic([v,w], poseData, predictTime);
               try
                    prob_collision = getOccupancy(costmap, next_pose(1:2));
               catch me %#ok<NASGU>
                    warning('me.message');
                    prob_collision = 0.5;
               end
               if prob_collision > 0.75
                   prob_collision = 0.9;
               end
               
               safety = 1-prob_collision;
               comfort = exp(-beta * abs(w - prevW));
               obedience = exp(-gamma * abs( angdiff ( u_ang , joy_ang)));
               
               current_set = [safety, comfort, obedience];
               current_min = min(current_set);
               
               if current_min > prev_best
                   prev_best = current_min;
                   w_blend = w;
                   best_k1 = k1;
               end               
            end
%             fprintf('user: %.1f bestSafety: %.3f bestComfort: %.3f bestObedience: %.3f \n',best_k1, best_set(1), best_set(2), best_set(3));
            fprintf('user: %.1f \n', best_k1);
            if(laser.mindist < obsThresh && v > 0)
                v = u_eband(1);
            end
            
            u_shared = [v, w_blend];
            user_weight = best_k1;
            prevW = w_blend;
            
        case 'Teleop'
           u_blend = u_joy;
           u_shared = u_joy;
           user_weight = 1;
            
        case 'CollisionAvoidance' 
            %Intervene only when user is going to collide
            u_shared = u_joy;
            
            alpha = 1; %Initial user weight
            deltaAlpha = 0.1; %Step to decrement user's weight in case of collision
            
            u_shared = alpha * u_joy + (1 - alpha) * u_eband;
            
            while (checkCollision(u_shared, 1, 0.9) && alpha > 0)
                if alpha > 0.1
                    alpha = alpha - deltaAlpha;
                else
                    alpha = 0;
                end
                u_shared =  (alpha * u_joy + (1 - alpha) * u_eband );
            end
            
            user_weight = alpha;
            

        % Prompting enabled    
        case 'CollisionFree'
            prompt_flag = true;
            goal_ang = simpleComputeTargetAngle( goal, poseData );
            
             u_blend = u_joy;
             joy_ang = atan2(u_joy(2), u_joy(1));
             if in_collision
                u_shared = [0,u_joy(2)];
                user_weight = 0;
                vfhAngle = rad2deg(step(vfh, laser.Ranges, laser.Angles, joy_ang));
                if vfhAngle < 0 
                    if prompt_flag && (~flag)
                        sound(handles.sounds.OR_sound.data,handles.sounds.OR_sound.fs);
                        time_called = clock;
                        flag = check_time(time_called, current_time, 5);
                    elseif (size(time_called) ~= 0)
                        flag = check_time(time_called, current_time, 5);  
                    end
                elseif (vfhAngle >= 0 && isnan(vfhAngle)==false)
                    if prompt_flag && (~flag)
                        sound(handles.sounds.OL_sound.data,handles.sounds.OL_sound.fs);
                        time_called = clock;
                        flag = check_time(time_called, current_time, 5);
                    elseif (size(time_called) ~= 0)
                        flag = check_time(time_called, current_time, 5);  
                    end
                end
             elseif near_collision
                 u_shared = [u_joy(1)*0.5, u_joy(2)];
                 user_weight = 0.5;
                 %Prompt for slowing down
                 %TODO
             else
                degrees = rad2deg(goal_ang);
                if(degrees <= 5 && degrees >= -5)
                    if prompt_flag && (~flag)
                        sound(handles.sounds.KS_sound.data,handles.sounds.KS_sound.fs);
                        time_called = clock;
                        flag = check_time(time_called, current_time, 10);  
                    elseif (size(time_called) ~= 0)
                        flag = check_time(time_called, current_time, 10);  
                    end
                elseif(degrees > 5)
                    % Go left
                    if prompt_flag && (~flag)
                        sound(handles.sounds.GL_sound.data,handles.sounds.GL_sound.fs);
                        time_called = clock;
                        flag = check_time(time_called, current_time, 10);  
                    elseif (size(time_called) ~= 0)
                        flag = check_time(time_called, current_time, 10);  
                    end
                else 
                    % Go right
                    if prompt_flag && (~flag)
                        sound(handles.sounds.GR_sound.data,handles.sounds.GR_sound.fs);
                        time_called = clock;
                        flag = check_time(time_called, current_time, 10);  
                    elseif (size(time_called) ~= 0)
                        flag = check_time(time_called, current_time, 10);  
                    end
                end
                u_shared = u_joy;
                user_weight = 1;
            end
        case 'NearGoalAutoSwitch'
            distThresh = 3.5;
%             [ebandPlan.Waypoints, ebandPlan.Dist] = extractWaypoints(handles.subpath.LatestMessage);
%             [nearGoal, ~] = checkNearGoal(goal, poseData, laser, ebandPlan, distThresh)
            nearGoal = norm(goal-poseData(1:2),2) < distThresh;
            
            if nearGoal
                u_shared= u_eband;   
                user_weight = 0;
            else
                u_blend = u_joy;
              
                if in_collision
                    u_shared = computeVFHvelocities(vfh, u_blend, laser);
                    user_weight = 0.5;
                else
                    u_shared = u_blend;
                    user_weight = 1;
                end
            end
                       
        case 'HighLevelBlend'
            %Blending with High Level Goal from Erdogan paper Steps away control from the
            %user based on euclidean distance to the goal. Perhaps should be plan
            %distance.
            dc = 5; %At what distance the control is at 50/50
            tau = 0.75;  %How fast the control steps away from the user, higher values produce steeper changes.
            
            d = norm(goal-poseData(1:2),2); %euclidean distance to goal
            alpha = 1 / (1 + exp(-tau*(d - dc))); %user weight
            deltaAlpha = min(alpha/5,0.1); %Step to decrement user weight in case of collision
            
            u_shared =  alpha * u_joy + (1 - alpha) * u_eband;
            
            while (checkCollision(u_shared, 1, 0.8) && alpha > 0)
                if alpha > deltaAlpha
                    alpha = alpha - deltaAlpha;
                else
                    alpha = 0;
                end
                u_shared =  (alpha * u_joy + (1 - alpha) * u_eband );
            end
            user_weight = alpha;
            
        case 'SimpleBlend'
            %Naive linear blending approach always giving same amount of control to user
            %and planner.
            
            alpha = 0.5;
            deltaAlpha = 0.1; %Step to decrement user's weight in case of collision
            
            u_shared = alpha * u_joy + (1 - alpha) * u_eband;
            
            while (checkCollision(u_shared, 1, 0.9) && alpha > 0)
                if alpha > 0.1
                    alpha = alpha - deltaAlpha;
                else
                    alpha = 0;
                end
                u_shared =  (alpha * u_joy + (1 - alpha) * u_eband );
            end
            user_weight = alpha;
            
        case 'AwayFromGoalBlend'
            %Blending similar to HighLevelBlend but inverse values for alpha. When the
            %user is away from the goal, the planner will have a higher weight. As the
            %user gets closer to the goal it gets more control.
            
            dc = 10; %At what distance the control is at 50/50
            tau = 0.75;  %How fast the control steps away from the planner, higher values produce steeper changes.
            
            d = norm(goal-poseData(1:2),2);
            alpha = 1 - (1 / (1 + exp(-tau*(d - dc)))); %user weight
            deltaAlpha = min(alpha/5,0.1); %Step to decrement user weight in case of collision
            
            u_shared =  (alpha * u_joy + (1-alpha) * u_eband );
            
            while (checkCollision(u_shared, 1, 0.8) && alpha > 0)
                if alpha > deltaAlpha
                    alpha = alpha - deltaAlpha;
                else
                    alpha = 0;
                end
                u_shared =  (alpha * u_joy + (1 - alpha) * u_eband );
            end
            user_weight = alpha;
            
        case 'TestStopCollision'
            [u_blend] = u_eband;
            if in_collision
                u_shared = computeVFHvelocities(vfh, u_blend, laser);
            else
                u_shared = u_blend;
            end
            user_weight = 1;
            
        case 'DisagreementBlend'
            %Blend according to a disagreement metric between the user and the planner
            %inputs. If user is consistent with the planner (maintains an average angular
            %difference below treshold angTresh) then the planner starts taking over. It
            %will take a high angular difference (maxDiff) between the user and the
            %planner for the user to be granted complete control again. For now the
            %blending only modifies the angular component of the velocity, keeping the
            %user in full control of the linear component at all times.
            
            minSamples =  30; %User will always be in control if number of angular difference samples is below this parameter.
            maxDiff = pi/2;   %If the angular difference between the planner and the user is higher than this parameter, the user will re-gain complete control.
            angTresh = pi/2;  %Minimum average angular difference to be maintained by the user to be considered 'in agreement' with the planner.
            
            
            ebandAngle = atan2(u_eband(2), u_eband(1));
            joy_ang = atan2(u_joy(2), u_joy(1));
            
            cDif = abs(angdiff(joy_ang,ebandAngle)); %Current angular difference between user and planner
            dataobj.addAngle( cDif );   %Add current angular difference to WCData object
            %fprintf('cDif = %.2f \t aDif = %.2f \t stdD = %.2f\n',cDif, dataobj.averageValue, dataobj.stdDevValue);   
            
            if (length(dataobj.DataHistory) < minSamples || dataobj.averageValue >= angTresh)
                %follow user command if disagreement is high or number of samples is too
                %low.
                disp('user in control');
                u_shared = u_joy; 
                alpha=1;
            else
                %User seems to be agreeing with the planner
                if(cDif > maxDiff)
                    fprintf('cDif = %.2f\t aVal = %.2f\t stdD= %.2f\n', cDif, dataobj.averageValue, dataobj.stdDevValue);
                    disp('user back in control');
                    dataobj.clearData();
                    u_shared = u_joy;
                    alpha=1;
                else
                    alpha = 0.48*dataobj.averageValue;
                    fprintf('avg = %.2f \t user weight = %.2f\n', dataobj.averageValue, alpha);
                    u_shared(1) = u_joy(1);
                    u_shared(2) =  (alpha * u_joy(2) + (1 - alpha) * u_eband(2));
                    
                end
            end
            
            %Decrement alpha iteratevly to avoid obstacles. 
            while (checkCollision(u_blend, 1, 0.9) && alpha > 0)
                if alpha > 0.1
                    alpha = alpha - 0.1;
                else
                    alpha = 0;
                end
                u_shared =  (alpha * u_joy + (1 - alpha) * u_eband );
            end
            user_weight = alpha;   
            
        case{'Woz_policy'}
            %Policy 2 described for the wizard of oz study. (steering
            %correction)
            d0 = 1; %distance (m) from obstacle at which the user loses control
            d1 = 1.5; %distance (m) from obstacle at which the user re-gains control
            laser.mindist
            if (laser.mindist < d0)
                wcState = wcStates.a0
            elseif (laser.mindist > d1)
                wcState = wcStates.a1
            end
            
            if (wcState == wcStates.a0)
                u_shared = [min(u_joy(1), u_eband(1)), u_eband(2)];
                alpha = 0;
            elseif(wcState == wcStates.a1)
                u_shared = u_joy;
                alpha = 1;
            end
            
            user_weight = alpha;
    end
    
   prevState = mode;
   prevGoal = goal;
end

function [u_free] = computeVFHvelocities(vfh, u, laserData)
%[u_free] = computeVFHvelocities(vfh, u, laserData)
% vfh: Vector field histogram object
% u: control input vector [v w]
% laserData: struct with fields Ranges and Angles
persistent angPID

%Initialize PID Controller
if isempty(angPID)
    angGains = struct('pgain',2.5,'dgain',-3,'igain',0,'maxwindup',0','setpoint',0);
    angPID = ExampleHelperPIDControl(angGains);
end

%Parameters
maxWController = 1; %The controller cap is high to react quickly to collisions.
minWController = 0.5; %The wheelchair does not move below this angluar speed

uAngle = atan2(u(2),u(1));
vfhAngle = step(vfh, laserData.Ranges, laserData.Angles, uAngle);
% segbotShowVFH(vfh);
if isnan(vfhAngle) 
    u_free = [0, u(2)];
else
    angleDiff = abs( angdiff(uAngle, vfhAngle) );
    if angleDiff > pi/4
        u_free(1) = u(1)*exp(-0.4*angleDiff);
    else
        u_free(1) = u(1);
    end
    
    u_free(2) = update(angPID, -vfhAngle);
    if (abs(u_free(2)) > maxWController)
        u_free(2) = maxWController*sign(u_free(2));
    end

end

if (abs(u_free(2)) < minWController)
    u_free(2) = minWController*sign(u_free(2));
end

end 

function [nearGoal, targetAngle] = checkNearGoal(goal, pose, laser,ebandPlan, nearGoalThresh)
%[nearGoal, targetAngle] = checkNearGoal(goal, pose, laser)
%Checks whether the robot is in the vicinity of the goal (5 meter radius)
%and also verifies that the shortest distance between the robot and the
%goal is clear of obstacles. 

%Parameters
if nargin < 5
    nearGoalThresh = 5;
end

d1 =  norm(goal-ebandPlan.Waypoints(end,:),2);
distToGoal = ebandPlan.Dist + d1;

% distToGoal = norm(goal-pose(1:2),2);
targetAngle = simpleComputeTargetAngle(goal, pose);

if (distToGoal < nearGoalThresh)        
    if(targetAngle < pi/2 && targetAngle > -pi/2)
        indices = find((laser.Angles > targetAngle - pi/12)  & (laser.Angles < targetAngle + pi/12));
        mindistTargetRegion = min(laser.Ranges(indices));  %#ok<FNDSB>
        if mindistTargetRegion > distToGoal 
            nearGoal = 1;           
            return
        end
    end
end
nearGoal = 0;

end

function testPoint = get_test_point(points)
    len = length(points);
    if(len <= 5)
        testPoint(1) = points(len,1);
        testPoint(2) = points(len,2);
    else
        testPoint(1) = points(6,1);
        testPoint(2) = points(6,2);
    end
end
