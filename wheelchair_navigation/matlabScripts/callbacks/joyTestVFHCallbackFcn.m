function joyTestVFHCallbackFcn(~, message, subLaser, subAMCL, vfh, plotobj)
%joyTestVFHCallbackFcn Uses VFH+ to compute obstacle free direction. 

laserData = processLaserMessage(subLaser.LatestMessage);
poseData = processAMCLMessage(subAMCL.LatestMessage);
plotobj.plotData(poseData, laserData.cartesian);

linearV = double(message.Axes(2)); %2
angularV= double(message.Axes(1)); %1

targetDir = atan2(angularV, linearV);
vfh(laserData.Ranges, laserData.Angles, targetDir)
% show(vfh)
segbotShowVFH(vfh);
end

function laserData = processLaserMessage(message)
%processLaserMessage - Process ROS message with laser scan data

laserData.cartesian = readCartesian(message) * [0 1; -1 0];
[laserData.Ranges, laserData.Angles] = ExtractRangeDataMod(message);
[laserData.mindist, minIndex] = min(laserData.Ranges(~isnan(laserData.Ranges)));
laserData.minAngle = laserData.Angles(minIndex);
end

function poseData = processAMCLMessage(message)
%processAMCLMessage - Process ROS message with localization data
global pose_data
if isempty(message)
    poseData = [0 0 0];
    return;
end

pos = message.Pose.Pose;
xpos = pos.Position.X;
ypos = pos.Position.Y;
quat = pos.Orientation;
angles = quat2eul([quat.W quat.X quat.Y quat.Z]);
theta = angles(1);
poseData = [xpos, ypos, theta];
pose_data = poseData;
end