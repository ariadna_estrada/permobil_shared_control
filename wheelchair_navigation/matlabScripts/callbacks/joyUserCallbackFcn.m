function joyUserCallbackFcn(~, message, cmd_vel)
%ebandCallbackFcn Takes twist message produced by eband planner and sends
%it to the wheelchair using a JoyControl/joystick message type

minSpeed = 0.15;

cmd_vel.msg.Linear = message.Axes(2);
cmd_vel.msg.Angular = message.Axes(1);

speed = norm([cmd_vel.msg.Linear, cmd_vel.msg.Angular]);

%Low speeds will engage the motors but will be unable to move the
%wheelchair causing a very annoying clicking sound. 
if (speed < minSpeed)
    cmd_vel.msg.Linear = 0;
    cmd_vel.msg.Angular = 0;
end

cmd_vel.msg.T = rostime('now');
send(cmd_vel.pub, cmd_vel.msg);
end

