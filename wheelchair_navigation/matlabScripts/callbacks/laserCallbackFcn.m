function laserCallbackFcn(~, message)
%laserCallbackFcn Function to retreive LaserScan type message and create
%laserData struct with fields: 
% -Ranges: vector size n with laser ranges
% -Angles: vector size n with the corresponding angle of each laser reading
% -mindist: minimum registered distance from the laser scan. only
%           considering obstacles in the robot's path. 
% -minAngle: angle (rad) at which the minimum laser reading is located
%
% The laserData struct is a global variable that can be used by the other
% matlab ros nodes.
% Ariadna Estrada Jun/04/2018

global laserData
% What angles (rad) do we consider to be in the front of the robot
frontMin = -0.87; 
frontMax = 0.87; 

[R, A] = ExtractRangeDataMod(message);
validIdx = isfinite(R) &  R >= message.RangeMin & R <= message.RangeMax;
laser.Ranges = R(validIdx);
laser.Angles = A(validIdx);

%Find the closest obstacle in the path of the robot. A 100deg span centered
%at the robot front orientation.
frontIdx = laser.Angles > frontMin & laser.Angles < frontMax;
frontAngles = laser.Angles(frontIdx);
[laser.mindist, minIndex] = min(laser.Ranges(frontIdx));
laser.minAngle = frontAngles(minIndex);

laserData = laser;
end
