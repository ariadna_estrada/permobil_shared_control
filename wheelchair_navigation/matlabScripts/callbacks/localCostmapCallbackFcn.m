function localCostmapCallbackFcn(~,message)
%Script to retreive costmap from ROS and create an occupancy grid.
%The function shares information with the rest of the matlab ros
%nodes through global variables.
%Ariadna Estrada May/30/2018

global costmap

obstacle = getCostmap(message);
norm_cost = double(obstacle.cost) ./100;
map_cost = imrotate(norm_cost, 90);
costmap_grid = robotics.OccupancyGrid(map_cost, 20); %resolution is 20 cells / meter
costmap_grid.GridLocationInWorld = obstacle.origin;
% inflate(costmap_grid, 0.15);
costmap = costmap_grid;

end
