function odomCallbackFcn(~, message, odom)
%odomCallbackFcn Takes odometry topic published by hector_mapping and
%republishes with a 'map' frame to be visualized in rviz.

odom.msg = message;
odom.msg.Header.FrameId = 'map';



send(odom.pub, odom.msg);
end

