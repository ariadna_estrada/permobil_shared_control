function stopCallback( ~, ~, collisionNode)
    %Callback function called when timer is halted
    disp('Shutting down collision checker node')
    delete(collisionNode);
    disp('Shutting down the ROS node')
    rosshutdown
end