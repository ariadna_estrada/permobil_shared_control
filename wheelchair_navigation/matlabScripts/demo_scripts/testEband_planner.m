clear;close all;


rosshutdown;
rosinit('localhost');

 %% Create ROS publisher for sending out velocity commands
[vel_wc.pub, vel_wc.msg] = rospublisher('/cmd_vel', 'JoyControl/joystick');

 %% Subscribe to ROS topics
subEband = rossubscriber('/eband_cmd_vel',{@ebandTestCallbackFcn, vel_wc});




