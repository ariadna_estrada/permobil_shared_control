% clear;close all;
%% To test connection run in terminal
% roslaunch JoyControl wheelchair_shared_control.launch

rosshutdown;
rosinit('localhost');


 %% Create ROS publisher for sending out velocity commands
[vel_wc.pub, vel_wc.msg] = rospublisher('/cmd_vel', 'JoyControl/joystick');

 %% Subscribe to ROS topics
sub_user_joy = rossubscriber('/joy_user', {@joyUserCallbackFcn, vel_wc});



























