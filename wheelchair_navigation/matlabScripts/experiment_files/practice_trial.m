%% Script to enable practice run during experiment. 
% No modification of the user's input. To be used as a demonstration of the experiment
% tasks and mitigate novely effects. 

clear;close all;
load('sounds.mat');
rosshutdown;
rosinit('localhost');

% Create ROS publisher for sending out velocity commands
[vel_wc.pub, vel_wc.msg] = rospublisher('/cmd_vel', 'JoyControl/joystick');

% Subscribe to user joystick 
sub_user_joy = rossubscriber('/joy_user', {@joyUserCallbackFcn, vel_wc});

%% Play single bell sound
sound(sounds.goal_sound.data(1:42700, :), sounds.goal_sound.fs);


%% Play double bell sound
sound(sounds.goal_sound.data(43000:end, :), sounds.goal_sound.fs);




