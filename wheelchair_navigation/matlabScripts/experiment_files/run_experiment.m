function varargout = run_experiment(varargin)
% RUN_EXPERIMENT MATLAB code for run_experiment.fig
%      RUN_EXPERIMENT, by itself, creates a new RUN_EXPERIMENT or raises the existing
%      singleton*.
%
%      H = RUN_EXPERIMENT returns the handle to a new RUN_EXPERIMENT or the handle to
%      the existing singleton*.
%
%      RUN_EXPERIMENT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in RUN_EXPERIMENT.M with the given input arguments.
%
%      RUN_EXPERIMENT('Property','Value',...) creates a new RUN_EXPERIMENT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before run_experiment_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to run_experiment_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help run_experiment

% Last Modified by GUIDE v2.5 03-Sep-2018 12:07:20

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @run_experiment_OpeningFcn, ...
                   'gui_OutputFcn',  @run_experiment_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before run_experiment is made visible.
function run_experiment_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject  t');  handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to run_experiment (see VARARGIN)


%load task files
load('lab_20180926.mat');
load('block_order.mat');
load('trajectories.mat');
load('sounds.mat');

ax = gca;
show(map);
title('World map');
hold(ax, 'on');
ax.YAxisLocation = 'right';
grid on;

%
current_task = 1;
current_task_goal = 1;
current_participant = handles.current_participant;
current_mode = block_order.mode{block_order.(handles.current_block)(current_participant,current_task)};
current_room = block_order.room_order.(handles.current_block){current_participant, current_task};
%
set(handles.current_mode_label, 'String', current_mode)
set(handles.current_task_label, 'String', num2str(current_task));
set(handles.goal12_label, 'String', num2str(current_task_goal));
%
s_pos = trajectories.(current_mode).(current_room).start;
e_pos = trajectories.(current_mode).(current_room).end;
p_pos = trajectories.(current_mode).(current_room).planner;
set(handles.user_x, 'String', num2str(e_pos(1)));
set(handles.user_y, 'String', num2str(e_pos(2)));
set(handles.planner_x, 'String', num2str(p_pos(1)));
set(handles.planner_y, 'String', num2str(p_pos(2)));
%
plots(1) = plot(ax, s_pos(1), s_pos(2), 'g.', 'MarkerSize', 30); %start location
% plots(2) = plot(ax, e_pos(1), e_pos(2), 'rp', 'MarkerSize', 10, 'MarkerFaceColor', 'r'); %user end location
% plots(3) = plot(ax, p_pos(1), p_pos(2), 'bp', 'MarkerSize', 5,  'MarkerFaceColor', 'b'); %planner end location

%
handles.block_order = block_order;
handles.trajectories = trajectories;
handles.current_goals = trajectories.(current_mode).(current_room);
handles.current_mode = current_mode;
handles.current_task = current_task;
handles.current_goal = current_task_goal;
handles.plots = plots;
handles.sounds = sounds;
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes run_experiment wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = run_experiment_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in block_popup.
function block_popup_Callback(hObject, eventdata, handles)
% hObject    handle to block_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns block_popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from block_popup

current_block = hObject.String{hObject.Value};
%
block_order = handles.block_order;
trajectories = handles.trajectories;
plots = handles.plots;
%
current_task = handles.current_task;
current_task_goal = handles.goal12_label.String;
current_participant = handles.current_participant;
current_room = block_order.room_order.(current_block){current_participant, current_task};
%
current_mode = block_order.mode{block_order.(current_block)(current_participant,current_task)};
%
s_pos = trajectories.(current_mode).(current_room).start;
if strcmp(current_task_goal, '1')
    e_pos = trajectories.(current_mode).(current_room).end;
    p_pos = trajectories.(current_mode).(current_room).planner;
else
    e_pos = trajectories.(current_mode).(current_room).end_2;
    p_pos = trajectories.(current_mode).(current_room).planner_2;
end
set(handles.user_x, 'String', num2str(e_pos(1)));
set(handles.user_y, 'String', num2str(e_pos(2)));
set(handles.planner_x, 'String', num2str(p_pos(1)));
set(handles.planner_y, 'String', num2str(p_pos(2)));
%
set(plots(1), 'xdata', s_pos(1), 'ydata', s_pos(2));
% set(plots(2), 'xdata', e_pos(1), 'ydata', e_pos(2));
% set(plots(3), 'xdata', p_pos(1), 'ydata', p_pos(2));
set(handles.current_mode_label, 'String', current_mode)
%

handles.current_block = current_block;
handles.current_mode = current_mode;
handles.current_goals = trajectories.(current_mode).(current_room);

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function block_popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to block_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
handles.block_popup = hObject;
handles.current_block = hObject.String{hObject.Value};
% Update handles structure
guidata(hObject, handles);


% --- Executes on selection change in participant_popup.
function participant_popup_Callback(hObject, eventdata, handles)
% hObject    handle to participant_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns participant_popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from participant_popup
current_participant = hObject.Value;
%
block_order = handles.block_order;
trajectories = handles.trajectories;
plots = handles.plots;
%
current_task = handles.current_task;
current_block = handles.current_block;
current_room = block_order.room_order.(current_block){current_participant, current_task};
%
current_mode = block_order.mode{block_order.(current_block)(current_participant,current_task)};
%
s_pos = trajectories.(current_mode).(current_room).start;
e_pos = trajectories.(current_mode).(current_room).end;
p_pos = trajectories.(current_mode).(current_room).planner;
set(handles.user_x, 'String', num2str(e_pos(1)));
set(handles.user_y, 'String', num2str(e_pos(2)));
set(handles.planner_x, 'String', num2str(p_pos(1)));
set(handles.planner_y, 'String', num2str(p_pos(2)));
%
set(plots(1), 'xdata', s_pos(1), 'ydata', s_pos(2));
% set(plots(2), 'xdata', e_pos(1), 'ydata', e_pos(2));
% set(plots(3), 'xdata', p_pos(1), 'ydata', p_pos(2));
set(handles.current_mode_label, 'String', current_mode)
%

handles.current_participant = current_participant;
handles.current_mode = current_mode;
handles.current_goals = trajectories.(current_mode).(current_room);

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function participant_popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to participant_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
handles.participant_popup = hObject;
handles.current_participant = hObject.Value;
% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in begin_tb.
function begin_tb_Callback(hObject, eventdata, handles)
% hObject    handle to begin_tb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of begin_tb
btn = get(hObject, 'Value');
%
block_order = handles.block_order;
trajectories = handles.trajectories;
plots = handles.plots;
%
current_task = handles.current_task;
current_block = handles.current_block;
current_participant = handles.current_participant;
%
if btn
    set(hObject, 'String', 'Complete trial');
    set(handles.participant_popup, 'enable', 'off');
else
    set(hObject, 'String', 'Begin trial');
    if current_task < length(block_order.mode)
        current_task = current_task + 1;
    else
        f = msgbox({'Current block completed'; 'Start next block'});
        current_task = 1;
    end
    set(handles.current_task_label, 'String', num2str(current_task));
    set(handles.goal12_label, 'String', '1');
end
%
current_room = block_order.room_order.(current_block){current_participant, current_task};
current_mode = block_order.mode{block_order.(current_block)(current_participant,current_task)};
current_task_goal = handles.goal12_label.String;
%
s_pos = trajectories.(current_mode).(current_room).start;
if strcmp(current_task_goal, '1')
    e_pos = trajectories.(current_mode).(current_room).end;
    p_pos = trajectories.(current_mode).(current_room).planner;
else
    e_pos = trajectories.(current_mode).(current_room).end_2;
    p_pos = trajectories.(current_mode).(current_room).planner_2;
end
set(handles.user_x, 'String', num2str(e_pos(1)));
set(handles.user_y, 'String', num2str(e_pos(2)));
set(handles.planner_x, 'String', num2str(p_pos(1)));
set(handles.planner_y, 'String', num2str(p_pos(2)));
%
set(plots(1), 'xdata', s_pos(1), 'ydata', s_pos(2));
% set(plots(2), 'xdata', e_pos(1), 'ydata', e_pos(2));
% set(plots(3), 'xdata', p_pos(1), 'ydata', p_pos(2));
set(handles.current_mode_label, 'String', current_mode)
%
handles.current_task = current_task;
handles.current_mode = current_mode;
handles.current_goals = trajectories.(current_mode).(current_room);

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in rosinit_pb.
function rosinit_pb_Callback(hObject, eventdata, handles)
% hObject    handle to rosinit_pb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
rosshutdown;
try
    rosinit('localhost');
catch
    warning('Unable to connect to ROS master');
    return;
end

% Create publishers and subscribers
try
    %Create rosservice to clear costmaps
    handles.clearCostmap = rossvcclient('move_base/clear_costmaps');
   
    %Set up publishers for commanded velocity and planner goal
    [handles.Vel.pub, handles.Vel.msg] = rospublisher('/cmd_vel', 'JoyControl/joystick');
    [handles.actionClient, handles.msgGoal] = rosactionclient('/move_base');
    handles.msgGoal.TargetPose.Header.FrameId = 'map';
    handles.actionClient.FeedbackFcn = [];
    
    %Set up publishers to be collected by rosbag
    [handles.intervention.pub, handles.intervention.msg] = rospublisher('/intervention', 'std_msgs/String', 'IsLatching', false);
    [handles.weight.pub, handles.weight.msg] = rospublisher('/user_weight', 'std_msgs/Float64');
    [handles.u_joy.pub, handles.u_joy.msg] = rospublisher('/u_joy', 'geometry_msgs/TwistStamped');
    [handles.u_robot.pub, handles.u_robot.msg] = rospublisher('/u_robot', 'geometry_msgs/TwistStamped');
    
    %Set up subscribers with callbacks
    handles.sublaser = rossubscriber('/scan_multi', @laserCallbackFcn);
    handles.subamcl = rossubscriber('/amcl_pose', @amclCallbackFcn);
    handles.subeband = rossubscriber('/eband_cmd_vel', @ebandCallbackFcn);
    handles.subcostmap= rossubscriber('/move_base/local_costmap/costmap', @localCostmapCallbackFcn);
    
    %Set up subscribers without callbacks
    handles.subpath = rossubscriber('/move_base/EBandPlannerROS/global_plan');
    handles.substatus = rossubscriber('/move_base/status');
    
    
    %Setup joystick callback (main callback fcn)
    clear joyExperimentSCCallbackFcn;
    handles.subjoy = rossubscriber('/joy_user', {@joyExperimentSCCallbackFcn, handles});
catch me
    disp('error creating publishers/subscribers');
    rosshutdown;
end
% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in clearCostmap_pb.
function clearCostmap_pb_Callback(hObject, eventdata, handles)
% hObject    handle to clearCostmap_pb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
call(handles.clearCostmap);

% --- Executes during object deletion, before destroying properties.
function figure1_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
 disp('Shutting down the ROS node')
 rosshutdown


% --- Executes on button press in mode_checkbox.
function mode_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to mode_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of mode_checkbox
show_mode = get(hObject, 'Value');

if show_mode 
    handles.current_mode_label.Visible = 'on';
else
    handles.current_mode_label.Visible = 'off';
end
