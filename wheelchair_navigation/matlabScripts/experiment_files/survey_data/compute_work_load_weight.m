%compute_work_load_weight: compute the weight of each source of workload
%using the measures from NASA-TLX
function work_load_weight = compute_work_load_weight(response_vector)
    load('cards.mat');
    ind = linspace(1,12,12);
    count = zeros(4,1);
    response = logical(zeros(1,12));
    
    for i=1:length(response_vector)
        response(i) = strcmp(response_vector{1,i}, 'On');
    end
    answers = ind(response);
    for i = 1:6
        response_string = cards{1, answers(i)};
        
        switch response_string
            case 'Mental Demand'
                count(1) = count(1) + 1;
            case 'Temporal Demand'
                count(2) = count(2) + 1;
            case 'Performance'
                count(3) = count(3) + 1;
            case 'Frustration'
                count(4) = count(4) + 1;
            otherwise
                warning('Unexpected source of workload type.')
        end
    end
    assert(sum(count) == 6, 'miscounting in source of workload')
    work_load_weight = count;
end
