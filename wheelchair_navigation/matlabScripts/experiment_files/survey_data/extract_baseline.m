function [baseline_struct] = extract_baseline(baseline_row)
%extract_baseline Extracts values from baseline questionnaire and stores it
%in a struct

load('moods.mat');

self_range = 1:3;
wc_range = 4:6;
mood_range = 7:26;
reported_mood = baseline_row(1,mood_range);
logical_mood = zeros(20, 1);
baseline_mood = [];

self_epa = table2array(baseline_row(1,self_range));
wc_epa = table2array(baseline_row(1,wc_range));


for i=1:length(logical_mood)
    logical_mood(i) = strcmp(reported_mood{1,i}, 'On');
    if logical_mood(i)
        if isempty(baseline_mood)
            baseline_mood = mood_words{i};
        else
            baseline_mood = {baseline_mood, mood_words{i}};
        end
    end
end

mood_array = logical_mood .* epa_mood;


baseline_struct.self_epa = self_epa;
baseline_struct.wc_epa = wc_epa;
baseline_struct. baseline_mood = baseline_mood;
baseline_struct.mood_array = mood_array;
end
