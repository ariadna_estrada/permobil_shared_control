function [mood_array, mood_cell] = extract_mood(mood_row)
%extract_mood extracts and stores elicited mood 
% mood_array: 20x6 array containing epa values of reported moods
% mood_cell: cell containing words corresponding to the reported moods

load('moods.mat');

logical_mood = zeros(20, 1);
mood_cell = [];

for i=1:length(logical_mood)
    logical_mood(i) = strcmp(mood_row{1,i}, 'On');
    if logical_mood(i)
        word = mood_words{i};
        
        if isempty(mood_cell)
            mood_cell = {word};
        else
            mood_cell = {mood_cell{:}, word};
        end
    end
end

mood_array = logical_mood .* epa_mood;

end

