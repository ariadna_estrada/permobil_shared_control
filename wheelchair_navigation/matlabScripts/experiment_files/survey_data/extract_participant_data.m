%extract_participant_data: construct a struct that stores the information
%of one participant's experiment data
%table_row: a row that corresponds to one participant from the survey table
%name_row_A: a row vector that stores the mode names of trial1
%name_row_B: a row vector that stores the mode names of trial2
function participant_struct = extract_participant_data(table_row, name_row_A, name_row_B, baseline_row)

    %Modifiable index and range that corresponds to data cells
    question_start_index = 18;
    block_size = 31;%Change only if questions were removed/added in each block
    behaviour_evaluation_range = 1:3;
    self_evaluation_range = 4:23;
    work_load_range = 24:27;
    safety_index = 28;
    difficulty_index = 29;
    Q1_index = 30;
    Q2_index = 31;
    num_blocks = 12;%Change only if more/fewer blocks are used
    
    %Extract baseline values
    [participant_struct.baseline_values] = extract_baseline(baseline_row(1,18:end));
    
    %Demographics Extraction
    %Construct workload_weight field
    demographics_start = question_start_index + num_blocks * block_size;%starting index of demographics block
    workLoad_weight_range = 0:11;%range of workload weight cards within demographics block
    participant_struct.workload_weight = compute_work_load_weight(table2array(table_row(1,demographics_start+workLoad_weight_range)));
    
    %Construct age field
    age_index = workLoad_weight_range(end)+1;%participant age question index within demographics block
    participant_struct.age = double(table2array(table_row(1,demographics_start+age_index)));
    
    %Construct gender field
    gender_index = age_index+1;%participant gender question index within demographics block
    gender_value = table2array(table_row(1,demographics_start+gender_index));
    switch gender_value
        case 1
            participant_struct.gender = char('Male');
        case 2
            participant_struct.gender = char('Female');
        case 3
            participant_struct.gender = char('Non-binary');
        case 4
            participant_struct.gender = char('Self-described');
        case 5
            participant_struct.gender = char('Prefer not to say');
        otherwise
            warning('Unexpected gender type.')
    end
    
    %Construct occupation field
    occupation_index = gender_index+2;%participant occupation question index within demographics block
    participant_struct.occupation = char(table2array(table_row(1,demographics_start+occupation_index)));
    
    %Construct mother tongue field
    mother_tongue_index = occupation_index+1;%participant first language question index within demographics block
    participant_struct.mother_tongue = char(table2array(table_row(1,demographics_start+mother_tongue_index)));
    
    %Construct robot interaction field
    robot_interaction_index = mother_tongue_index+1;%robot interaction question index within demographics block
    participant_struct.robot_interaction = table2array(table_row(1,demographics_start+robot_interaction_index));
    
    %Construct experience with pwc field
    experience_with_pwc_index = robot_interaction_index+1;%pwc experience question index within demographics block
    participant_struct.experience_with_pwc = table2array(table_row(1,demographics_start+experience_with_pwc_index));
    
    %Construct time spent with pwc field
    time_spent_index = experience_with_pwc_index+1;%time spent with pwc question index within demographics block
    participant_struct.time_with_pwc = char(table2array(table_row(1,demographics_start+time_spent_index)));
    
    %Construct EPA and written response fields
    for m = name_row_A
        %Find the block index that contains responses for each mode
        block_numA = find(name_row_A == m);
        block_numB = num_blocks/2 + find(name_row_B == m);
        
        %Construct EPA_wc field for each mode
        EPA_wc_rangeA = question_start_index + (block_numA-1)*block_size + behaviour_evaluation_range - 1;
        EPA_wc_rangeB = question_start_index + (block_numB-1)*block_size + behaviour_evaluation_range - 1;
        EPA_wc = [table2array(table_row(1,EPA_wc_rangeA));table2array(table_row(1,EPA_wc_rangeB))];
        participant_struct.(m).EPA_wc = EPA_wc;
        
        %Construct mood related fields for each mode
        EPA_self_rangeA = question_start_index + (block_numA-1)*block_size + self_evaluation_range - 1;
        EPA_self_rangeB = question_start_index + (block_numB-1)*block_size + self_evaluation_range - 1;
        [mood_array1, mood_words1] = extract_mood(table_row(1,EPA_self_rangeA)); 
        [mood_array2, mood_words2] = extract_mood(table_row(1,EPA_self_rangeB)); 
        participant_struct.(m).mood_array = [mood_array1; mood_array2];
        participant_struct.(m).mood_words = {mood_words1, mood_words2};

        %Construct workload field for each mode
        workload_rangeA = question_start_index + (block_numA-1)*block_size + work_load_range -1;
        workload_rangeB = question_start_index + (block_numB-1)*block_size + work_load_range -1;
        workload = [table2array(table_row(1,workload_rangeA));table2array(table_row(1,workload_rangeB))];
        participant_struct.(m).workload = workload;
        participant_struct.(m).weighted_tlx = (workload * participant_struct.workload_weight) * (5/6);
        
        %Construct safety field for each mode
        safety_indexA = question_start_index + (block_numA-1)*block_size + safety_index - 1;
        safety_indexB = question_start_index + (block_numB-1)*block_size + safety_index - 1;
        safety = [table2array(table_row(1,safety_indexA));table2array(table_row(1,safety_indexB))];
        participant_struct.(m).safety = safety;
        
        %Construct difficulty field for each mode
        difficulty_indexA = question_start_index + (block_numA-1)*block_size + difficulty_index - 1;
        difficulty_indexB = question_start_index + (block_numB-1)*block_size + difficulty_index - 1;
        difficulty = [table2array(table_row(1,difficulty_indexA));table2array(table_row(1,difficulty_indexB))];
        participant_struct.(m).difficulty = difficulty;
        
        %Construct Q1 and Q2 field for each mode
        Q1_indexA = question_start_index + (block_numA-1)*block_size + Q1_index - 1;
        Q1_indexB = question_start_index + (block_numB-1)*block_size + Q1_index - 1;
        
        Q2_indexA = question_start_index + (block_numA-1)*block_size + Q2_index - 1;
        Q2_indexB = question_start_index + (block_numB-1)*block_size + Q2_index - 1;
        
        Q1 = {table2cell(table_row(1,Q1_indexA)); table2cell(table_row(1,Q1_indexB))};
        Q2 = {table2cell(table_row(1,Q2_indexA)); table2cell(table_row(1,Q2_indexB))};
        participant_struct.(m).Q1 = Q1;
        participant_struct.(m).Q2 = Q2;
        
    end
end