function extract_survey(survey_file, baseline_file)
%extract_survey: extracts an input study survey CSV file into .mat
% Input
%   - survey_file: path to the post interaction survey csv file
%   - baseline_file: path to the baseline questionnaire csv file
%
%Outputs
%   - intervention_modes.mat: binary containing a struct with a collection
%   of the data from all participants separated by intervention
%   - participants.mat: binary containig a struct with the ratins separated
%   by participant
%

    %loading .csv file
    opts = detectImportOptions(survey_file);
    survey_table = readtable(survey_file, opts);
    num_participants = size(survey_table,1);
    
    %load baseline file
    opts2 = detectImportOptions(baseline_file);
    baseline_table = readtable(baseline_file, opts2);
    
    
    
    %loading predefined experiment configuration struct
    load('block_order.mat', 'block_order');
    driving_modes = string(block_order.mode);
    order_matrix_A = block_order.A;
    order_matrix_B = block_order.B;
    
    %construct matrices that contain mode names in the order
    %specified in order matrices
    name_matrix_A = string(order_matrix_A);
    name_matrix_B = string(order_matrix_B);
    
    assert((size(order_matrix_A, 1) == size(order_matrix_B, 1)) && ...
        (size(order_matrix_A, 2) == size(order_matrix_B, 2)), ...
        'The dimensions of 2 order matrices do not agree');
    
    for i = 1:size(name_matrix_A,1)
        for j = 1:size(name_matrix_A,2)
            name_matrix_A(i,j) = driving_modes(order_matrix_A(i,j));
            name_matrix_B(i,j) = driving_modes(order_matrix_B(i,j));
        end
    end
    
    %Construct the struct that stores each participant's experiment data
    for i = 1:num_participants
        str = strcat('P', num2str(i));
        participants.(str) = extract_participant_data(survey_table(i,:), ...
            name_matrix_A(i,:), ...
            name_matrix_B(i,:), baseline_table(i,:));
    end
    
    save('participants.mat', 'participants');
    
    %Construct the struct that stores each driving modes' data using all the participants'
    %data.
    %Initialize the intervention modes struct
    for mode = driving_modes
        for i = 1:num_participants
            intervention_modes.(mode).EPA_wc = [];
%             intervention_modes.(mode).EPA_self = [];
            intervention_modes.(mode).workload = [];
            intervention_modes.(mode).weighted_tlx = [];
            intervention_modes.(mode).safety = [];
            intervention_modes.(mode).difficulty = [];
            intervention_modes.(mode).Q1 = [];
            intervention_modes.(mode).Q2 = [];
        end
    end
    
    %Fill the intervention modes struct with data from participants
    for mode = driving_modes
        for i = 1:num_participants
            str = strcat('P', num2str(i));
            intervention_modes.(mode).EPA_wc = [intervention_modes.(mode).EPA_wc;...
                participants.(str).(mode).EPA_wc];
%             intervention_modes.(mode).EPA_self = [intervention_modes.(mode).EPA_self;...
%                 participants.(str).(mode).EPA_self];
            intervention_modes.(mode).workload = [intervention_modes.(mode).workload;...
                participants.(str).(mode).workload];
            intervention_modes.(mode).weighted_tlx = [intervention_modes.(mode).weighted_tlx;...
                participants.(str).(mode).weighted_tlx];
            intervention_modes.(mode).safety = [intervention_modes.(mode).safety;...
                participants.(str).(mode).safety];
            intervention_modes.(mode).difficulty = [intervention_modes.(mode).difficulty;...
                participants.(str).(mode).difficulty];
            intervention_modes.(mode).Q1 = [intervention_modes.(mode).Q1;...
                participants.(str).(mode).Q1];
            intervention_modes.(mode).Q2 = [intervention_modes.(mode).Q2;...
                participants.(str).(mode).Q2];
        end
    end
    
     save('intervention_modes.mat', 'intervention_modes');

end

