function [Ranges, Angles] = ExtractRangeDataMod(LaserScanMsg)
%#codegen

% Extract Range information
Ranges = double(LaserScanMsg.Ranges);

% Construct Angles vector using AngleMin and AngleIncrement from sensor
% reading.
angMin = double(LaserScanMsg.AngleMin);
angIncrement = double(LaserScanMsg.AngleIncrement);

numReadings = numel(Ranges);
rawAngles = angMin + (0:numReadings-1)' * angIncrement;

% Wrap the angles to the (-pi,pi] interval.
Angles = robotics.internal.wrapToPi(double(rawAngles));
end