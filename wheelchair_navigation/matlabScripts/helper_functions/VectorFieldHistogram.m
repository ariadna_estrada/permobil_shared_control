classdef VectorFieldHistogram < matlab.System & ...
        matlab.system.mixin.internal.CustomIcon & ...
        matlab.system.mixin.Propagates
    %VECTORFIELDHISTOGRAM Avoid obstacles using vector field histogram
    %   The vector field histogram (VFH) algorithm is used for obstacle
    %   avoidance based on range sensor data. Given a range sensor reading
    %   in terms of ranges and angles, and a target direction to drive
    %   towards, the VectorFieldHistogram computes an obstacle-free steering
    %   direction using the VFH+ algorithm.
    %
    %   VFH = robotics.VectorFieldHistogram returns a vector field histogram
    %   object, VFH, that computes a steering direction using the VFH+ algorithm.
    %
    %   VFH = robotics.VectorFieldHistogram('PropertyName', PropertyValue, ...)
    %   returns a vector field histogram object, VFH, with each specified
    %   property set to the specified value.
    %
    %   Step method syntax:
    %
    %   STEERINGDIR = step(VFH, RANGES, ANGLES, TARGETDIR) finds an obstacle
    %   free steering direction STEERINGDIR, using the VFH+ algorithm for
    %   input vectors RANGES and ANGLES of the same number of elements, and
    %   scalar input TARGETDIR. The input RANGES are in meters, the
    %   ANGLES and TARGETDIR are in radians. The output STEERINGDIR is in
    %   radians. The robot's forward direction is considered zero radians.
    %   The angles measured clockwise from the forward direction are negative
    %   angles and angles measured counter-clockwise from the forward direction
    %   are positive angles.
    %
    %   System objects may be called directly like a function instead of using
    %   the step method. For example, y = step(obj, x) and y = obj(x) are
    %   equivalent.
    %
    %   VectorFieldHistogram methods:
    %
    %   step        - Compute steering direction using the range data
    %   clone       - Create VectorFieldHistogram object with same property values
    %   show        - Display VectorFieldHistogram information in a figure window
    %   reset       - Reset the internal states of VectorFieldHistogram System object
    %
    %   VectorFieldHistogram properties:
    %
    %   NumAngularSectors       - Number of angular sectors in histogram
    %   DistanceLimits          - Ranges within these limits are considered
    %   RobotRadius             - Radius of the circle circumscribing the robot
    %   SafetyDistance          - Safety distance around the robot
    %   MinTurningRadius        - Minimum turning radius at current speed
    %   TargetDirectionWeight   - Weight for moving in target direction
    %   CurrentDirectionWeight  - Weight for moving in current direction
    %   PreviousDirectionWeight - Weight for moving in previous direction
    %   HistogramThresholds     - Upper and lower thresholds for histogram
    %
    %   Example:
    %
    %       % Create a vector field histogram object
    %       vfh = robotics.VectorFieldHistogram;
    %
    %       % Example laser scan data input
    %       ranges = 10*ones(1, 300);
    %       ranges(1, 130:170) = 1.0;
    %       angles = linspace(-pi/2, pi/2, 300);
    %       targetDir = 0;
    %
    %       % Compute obstacle-free steering direction
    %       steeringDir = step(vfh, ranges, angles, targetDir)
    %
    %       % Visualize the VectorFieldHistogram computation
    %       show(vfh);
    %
    %   See also robotics.PurePursuit, robotics.PRM.
    
    %   Copyright 2015-2016 The MathWorks, Inc.
    %
    %   References:
    %
    %   [1] I. Ulrich and J. Borenstein, "VFH+: reliable obstacle avoidance
    %       for fast mobile robots", Proceedings of IEEE International
    %       Conference on Robotics and Automation, 1998.
    
    %#codegen
    %#ok<*EMCA>
    
    properties (Nontunable)
        %NumAngularSectors Number of angular sectors
        %   The number of angular sectors are the number of bins used to
        %   create histograms
        %
        %   Default: 180
        NumAngularSectors = 180
    end
    
    properties (Hidden)
        %NarrowOpeningThreshold Angular threshold in radians
        %   This is an angular threshold, specified in radians, to consider
        %   an angular region to be narrow. The algorithm selects one
        %   candidate direction for each narrow region, while it selects
        %   two candidate directions in each non-narrow region.
        %
        %   Default: 0.8
        NarrowOpeningThreshold = 0.8
    end
    
    properties
        %DistanceLimits Range distance limits (m)
        %   The range readings specified in the step function input are
        %   considered only if they fall within the distance limits.
        %   The lower distance limit is specified to ignore false
        %   positive data while the higher distance limit is specified
        %   to ignore obstacles that are too far from the robot.
        %
        %   Default: [0.05 2]
        DistanceLimits = [0.05, 2];
        
        %RobotRadius Robot radius (m)
        %   This is radius of the smallest circle that can circumscribe the
        %   robot geometry. The robot radius is used to account for robot
        %   size in the computation of the obstacle-free direction.
        %
        %   Default: 0.1
        RobotRadius = 0.1;
        
        %SafetyDistance Safety distance (m)
        %   This is a safety distance to leave around the robot position in
        %   addition to the RobotRadius. The robot radius and safety
        %   distance are used in the computation of the obstacle-free
        %   direction.
        %
        %   Default: 0.1
        SafetyDistance = 0.1;
        
        %MinTurningRadius Minimum turning radius (m)
        %   This is the minimum turning radius with which the robot can turn
        %   while moving at its current speed.
        %
        %   Default: 0.1
        MinTurningRadius = 0.1;
        
        %TargetDirectionWeight Target direction weight
        %   This is the cost function weight for moving towards the target
        %   direction. To follow a target direction, the
        %   TargetDirectionWeight should be higher than the sum of
        %   CurrentDirectionWeight and PreviousDirectionWeight. You can
        %   ignore the target direction cost by setting this weight to zero.
        %
        %   Default: 5
        TargetDirectionWeight = 5
        
        %CurrentDirectionWeight Current direction weight
        %   This is the cost function weight for moving in the current
        %   heading direction. Higher values of this weight produces
        %   efficient paths. You can ignore the current direction cost
        %   by setting this weight to zero.
        %
        %   Default: 2
        CurrentDirectionWeight = 2
        
        %PreviousDirectionWeight Previous direction weight
        %   This is the cost function weight for moving in the previously
        %   selected steering direction. Higher values of this weight
        %   produces smoother paths. You can ignore the previous direction
        %   cost by setting this weight to zero.
        %
        %   Default: 2
        PreviousDirectionWeight = 2
        
        %HistogramThresholds Histogram thresholds
        %   These thresholds are used to compute the binary histogram from
        %   the polar obstacle density. Polar obstacle density values higher
        %   than the upper threshold are considered to be occupied (1) in
        %   the binary histogram. Polar obstacle density values smaller
        %   than the lower threshold are considered to be free space (0) in
        %   the binary histogram. The values of polar obstacle density that
        %   fall between the upper and lower thresholds are determined by
        %   the previous binary histogram, with default being free space (0).
        %
        %   Default: [3 10]
        HistogramThresholds = [3 10]
    end
    
    properties(Access = {?robotics.VectorFieldHistogram, ...
            ?matlab.unittest.TestCase})
        %PolarObstacleDensity Polar obstacle density histogram
        PolarObstacleDensity
        
        %BinaryHistogram Binary polar histogram
        BinaryHistogram
        
        %MaskedHistogram Masked polar histogram
        MaskedHistogram
        
        %PreviousDirection Steering direction output of the last step call
        %
        %   Default: 0
        PreviousDirection
        
        %TargetDirection Target direction specified in the last step call
        %
        %   Default: 0
        TargetDirection
        
        %AngularSectorMidPoints Angular sectors in radians
        %   These are the angular sectors determined based on the angular
        %   limits and the number of angular sectors.
        AngularSectorMidPoints
        
        %IsShowBeforeStep Flag to prevent calls to show before step
        IsShowBeforeStep = true;
        
        %AngularDifference Size of each angular sector
        AngularDifference
        
        %AngularSectorStartPoints Start points of angular sectors
        AngularSectorStartPoints
        
        %AngularSectorEndPoints Start and end points for angular sectors
        AngularSectorEndPoints
    end
    
    properties (Access = private)
        %Ranges Range sensor reading from the last step call
        Ranges
        
        %Angles Angles corresponding to ranges from the last step call
        Angles
        
        %AngularLimits Minimum and maximum angular limits in radians
        %   A vector [MIN MAX] representing the angular limits to consider
        %   as candidate directions. This is usually the angular limits of
        %   the range sensor. If an empty value is assigned, the angular
        %   limits will be computed from the input angles in the step
        %   function.
        %
        %   Default: [-pi, pi]
        AngularLimits = [-pi, pi];
    end
    
    methods (Access = protected)
        function setupImpl(obj, ranges, ~, ~)
            %setupImpl Setup for the system object
            
            obj.PreviousDirection = cast(0, 'like', ranges);
            angularLimits = cast(obj.AngularLimits, 'like', ranges);
            numAngularSectors = cast(obj.NumAngularSectors, 'like', ranges);
            
            % Create angular sectors
            obj.AngularSectorMidPoints = linspace(angularLimits(1,1)+...
                pi/numAngularSectors, angularLimits(1,2)-...
                pi/numAngularSectors, numAngularSectors);
            
            if numAngularSectors > 1
                obj.AngularDifference = abs(robotics.internal.angdiff(...
                    obj.AngularSectorMidPoints(1,1), ...
                    obj.AngularSectorMidPoints(1,2)));
            else
                obj.AngularDifference = cast(2*pi, 'like', ranges);
            end
            
            obj.AngularSectorStartPoints = obj.AngularSectorMidPoints - ...
                obj.AngularDifference/2;
            sectorEndPoints = obj.AngularSectorMidPoints + ...
                obj.AngularDifference/2;
            sectorPoints = [obj.AngularSectorStartPoints; ...
                sectorEndPoints];
            obj.AngularSectorEndPoints = sectorPoints(:)';
            
            % Pre-allocate the histogram
            obj.BinaryHistogram = false(1, obj.NumAngularSectors);
        end
        
        
        function steeringDir = stepImpl(obj, ranges, angles, target)
            %step Compute control commands and steering direction
            %   STEERINGDIR = step(VFH, RANGES, ANGLES, TARGETDIR) finds an obstacle
            %   free steering direction STEERINGDIR, using the VFH+ algorithm for
            %   input vectors RANGES and ANGLES of the same number of elements, and
            %   scalar input TARGETDIR. The input RANGES are in meters, the
            %   ANGLES and TARGETDIR are in radians. The output STEERINGDIR is in
            %   radians. The robot's forward direction is considered zero radians.
            %   The angles measured clockwise from the forward direction are negative
            %   angles and angles measured counter-clockwise from the forward direction
            %   are positive angles.
            
            validateStepInputs(ranges, angles, target);            
            
            if max(abs(angles)) > pi
                angles = robotics.internal.wrapToPi(angles);
            end
            
            if abs(target) > pi
                target = robotics.internal.wrapToPi(target);
            end
            
            % Store ranges for plotting in simulation
            if isempty(coder.target)  % Simulation
                obj.Ranges = ranges(:);
                obj.Angles = angles(:);
            end
            
            % Compute theta steer
            obj.buildPolarObstacleDensity(ranges(:), angles(:));
            obj.buildBinaryHistogram;
            obj.buildMaskedPolarHistogram(ranges(:), angles(:));
            steeringDir = obj.selectHeadingDirection(target);
            
            % Allow show method to be called
            obj.IsShowBeforeStep = false;
        end
        
        function loadObjectImpl(obj, svObj, wasLocked)
            %loadObjectImpl Custom load implementation
            
            % Copy all private properties
            mco = ?robotics.VectorFieldHistogram;
            propList = mco.PropertyList;
            
            for i = 1:length(propList)
                if obj.isPrivateProperty(propList(i))
                    obj.(mco.PropertyList(i).Name) = ...
                        svObj.(mco.PropertyList(i).Name);
                end
            end
            
            % Call base class method
            loadObjectImpl@matlab.System(obj,svObj,wasLocked);
        end
        
        
        function s = saveObjectImpl(obj)
            %saveObjectImpl Custom save implementation
            s = saveObjectImpl@matlab.System(obj);
            
            % Save all private properties
            mco = ?robotics.VectorFieldHistogram;
            propList = mco.PropertyList;
            
            for i = 1:length(propList)
                if obj.isPrivateProperty(propList(i))
                    s.(mco.PropertyList(i).Name) = ...
                        obj.(mco.PropertyList(i).Name);
                end
            end
        end
        
        function validateInputsImpl(~, ranges, angles, target)
            %validateInputsImpl Validate inputs before setupImpl is called
            validateStepInputs(ranges, angles, target);
            
            isDataTypeEqual = isequal(class(ranges), class(angles), class(target));
            
            coder.internal.errorIf(~isDataTypeEqual, ...
                'robotics:robotalgs:vfh:DataTypeMismatch', ...
                class(ranges), class(angles), class(target));
        end
    
        
        function outSize = getOutputSizeImpl(~)
            %getOutputSizeImpl Return size for each output port
            
            % Steering direction type is scalar
            outSize = 1;
        end
        
        function outType = getOutputDataTypeImpl(obj)
            %getOutputDataTypeImpl Return data type for each output port
            
            outType = propagatedInputDataType(obj,1);
        end
        
        function outComplex = isOutputComplexImpl(~)
            %isOutputComplexImpl Return true for each output port with complex data
            
            % Steering direction is real
            outComplex = false;
        end
        
        function outFixedSize = isOutputFixedSizeImpl(~)
            %isOutputFixedSizeImpl Return true for each output port with fixed size
            
            % Steering direction is fixed size
            outFixedSize = true;
        end
        
        function [fs1, fs2, fs3] = isInputFixedSizeImpl(~)
            %isOutputFixedSizeImpl Return true for each output port with fixed size
            
            % Ranges is variable size
            fs1 = false;
            % Angles is variable size
            fs2 = false;
            % Target direction is fixed size
            fs3 = true;
        end
        
        function [name1, name2, name3] = getInputNamesImpl(~)
            %getInputNamesImpl Return input port names for System block
            name1 = 'Ranges';
            name2 = 'Angles';
            name3 = 'TargetDir';
        end
        
        function outNames = getOutputNamesImpl(~)
            %getOutputNamesImpl Return output port names for System block
            outNames = 'SteerDir';
        end
        
        function maskDisplay = getMaskDisplayImpl(obj)
            %getMaskDisplayImpl Customize the mask icon display
            %   This method is inherited from matlab.system.mixin.internal.CustomIcon
            %   and allows the customization of the mask display code. Note
            %   that this works both for the base mask and for the
            %   mask-on-mask.
            
            % Construct the input labels based no the number of inputs
            numInputs = obj.getNumInputsImpl;
            [inputNames{1:numInputs}] = obj.getInputNamesImpl;
            
            portLabelText = {};
            for i = 1:length(inputNames)
                portLabelText = [portLabelText ['port_label(''input'', ' num2str(i) ', ''' inputNames{i} ''')']]; %#ok<AGROW>
            end
            
            numOutputs = obj.getNumOutputsImpl;
            [outputNames{1:numOutputs}] = obj.getOutputNamesImpl;
            for i = 1:length(outputNames)
                portLabelText = [portLabelText ['port_label(''output'', ' num2str(i) ', ''' outputNames{i} ''')']]; %#ok<AGROW>
            end
            
            maskDisplay = { ...
                ['color(''white'');', char(10)], ...                                     % Fix min and max x,y co-ordinates for autoscale mask units
                ['plot([100,100,100,100],[100,100,100,100]);', char(10)], ...
                ['plot([0,0,0,0],[0,0,0,0]);', char(10)],...
                'color(''blue'')', ...
                'text(50, 95, ''Obstacle Avoidance'', ''horizontalAlignment'', ''center'')', ...
                'color(''black'')', ...
                'text(52, 50, ''Vector Field\n Histogram'', ''horizontalAlignment'', ''center'');', ...
                portLabelText{:}};  %#ok<CCAT>
        end
        
        function resetImpl(obj)
            %resetImpl Reset internal states
            
            obj.BinaryHistogram = false(1, obj.NumAngularSectors);
            obj.PreviousDirection = 0*obj.PreviousDirection;
            % Prevent calls to show method before step
            obj.IsShowBeforeStep = true;
        end
        
        function num = getNumInputsImpl(~)
            %getNumInputsImpl Get number of inputs
            num = 3;
        end
        
        function num = getNumOutputsImpl(~)
            %getNumOutputsImpl Define number of outputs for system with optional outputs
            num = 1;
        end
        
        function flag = isInputSizeLockedImpl(~,index)
            %isInputSizeLockedImpl Locked input size status
            %   This function will be called once for each input of the
            %   system block.
            %   First two inputs, i.e. ranges and angles are variable sized
            %   signals.
            if (index == 1 || index  == 2)
                flag = false;
            else
                flag = true;
            end
        end
    end
    
    methods(Static, Access = protected)
        % Note that this is ignored for the mask-on-mask
        function header = getHeaderImpl
            %getHeaderImpl Create mask header
            header = matlab.system.display.Header('robotics.VectorFieldHistogram', ...
                'Title', message('robotics:robotslalgs:vfh:VFHTitle').getString, ...
                'Text', message('robotics:robotslalgs:vfh:VFHDescription').getString, ...
                'ShowSourceLink', false);
        end
        
        function groups = getPropertyGroupsImpl
            %getPropertyGroupsImpl Display parameters with groups and tabs
            valueGroup = matlab.system.display.Section(...
                'Title','Histogram parameters', 'PropertyList', ...
                {'NumAngularSectors', 'DistanceLimits', 'HistogramThresholds'});
            
            thresholdGroup = matlab.system.display.Section(...
                'Title','Robot parameters', 'PropertyList', ...
                {'RobotRadius','SafetyDistance','MinTurningRadius'});
            
            mainGroup = matlab.system.display.SectionGroup(...
                'Title','Main', 'Sections',[valueGroup,thresholdGroup]);
            
            initGroup = matlab.system.display.SectionGroup(...
                'Title','Cost Function Weights', 'PropertyList', ...
                {'TargetDirectionWeight','CurrentDirectionWeight','PreviousDirectionWeight'});
            
            groups = [mainGroup,initGroup];
        end
    end
    
    methods
        function set.DistanceLimits(obj, val)
            validateNonnegativeArray(val, 'DistanceLimits');
            obj.DistanceLimits = [min(val) max(val)];
        end
        
        function set.NarrowOpeningThreshold(obj, val)
            validateattributes(val, {'double'}, {'nonnan', 'real', ...
                'scalar', 'positive', 'finite'}, 'VectorFieldHistogram',...
                'NarrowOpeningThreshold');
            obj.NarrowOpeningThreshold = val;
        end
        
        function set.RobotRadius(obj, val)
            validateNonnegativeScalar(val, 'RobotRadius');
            obj.RobotRadius = val;
        end
        
        function set.SafetyDistance(obj, val)
            validateNonnegativeScalar(val, 'SafetyDistance');
            obj.SafetyDistance = val;
        end
        
        function set.MinTurningRadius(obj, val)
            validateNonnegativeScalar(val, 'MinTurningRadius');
            obj.MinTurningRadius = val;
        end
        
        function set.TargetDirectionWeight(obj, val)
            validateNonnegativeScalar(val, 'TargetDirectionWeight');
            obj.TargetDirectionWeight = val;
        end
        
        function set.CurrentDirectionWeight(obj, val)
            validateNonnegativeScalar(val, 'CurrentDirectionWeight');
            obj.CurrentDirectionWeight = val;
        end
        
        function set.PreviousDirectionWeight(obj, val)
            validateNonnegativeScalar(val, 'PreviousDirectionWeight');
            obj.PreviousDirectionWeight = val;
        end
        
        function set.HistogramThresholds(obj, val)
            validateNonnegativeArray(val, 'HistogramThresholds');
            obj.HistogramThresholds = [min(val) max(val)];
        end
        
        function set.NumAngularSectors(obj, val)
            validateattributes(val, {'double', 'single'}, {'nonnan', 'integer', ...
                'scalar', 'positive', 'finite'}, 'VectorFieldHistogram', ...
                'NumAngularSectors');
            obj.NumAngularSectors = val;
        end
        
        function val = get.NumAngularSectors(obj)
            val = obj.NumAngularSectors;
        end
        
        function obj = VectorFieldHistogram(varargin)
            %VectorFieldHistogram Constructor
            setProperties(obj,nargin,varargin{:});
        end
    end
    
    methods
        function ax = show(obj, name, value)
            %show Show the histograms in a figure
            %   show(VFH) shows different histograms in a figure along with
            %   the parameters of the VectorFieldHistogram and range values
            %   from the last step input.
            %
            %   AH = show(VFH) returns handles of the axes used by the show
            %   function.
            %
            %   show(VFH,___,Name,Value) provides additional options specified
            %   by one or more Name,Value pair arguments. Name must appear
            %   inside single quotes (''). You can specify several name-value
            %   pair arguments in any order as Name1,Value1,...,NameN,ValueN:
            %
            %       'Parent'        - A two-element array of axes handles
            %                         that specifies the parent axes for
            %                         the objects created by show.
            %
            %   Example:
            %       % Create a vector field histogram
            %       vfh = robotics.VectorFieldHistogram;
            %
            %       % Example laser scan data
            %       ranges = 10*ones(1, 300);
            %       ranges(1, 130:170) = 1.0;
            %       angles = linspace(-pi/2, pi/2, 300);
            %       targetDir = 0;
            %
            %       % Compute control inputs and steering direction
            %       steeringDir = step(vfh, ranges, angles, targetDir)
            %
            %       % Visualize histograms using show
            %       show(vfh);
            %
            %       % Specify axes using Name-Value pair
            %       fh = figure;
            %       ax(1) = subplot(1,2,1);
            %       ax(2) = subplot(1,2,2);
            %       show(vfh, 'Parent', ax);
            %
            %   See also robotics.PurePursuit
            
            % If step has not been called then error out
            if obj.IsShowBeforeStep
                error(message('robotics:robotalgs:vfh:ShowBeforeStep'));
            end
            
            % Plot polar obstacle density histogram
            if nargin > 1
                validatestring(name,{'Parent'}, 'show', 'Name');
                validateattributes(value,{'matlab.graphics.axis.Axes'}, ...
                    {'numel', 2}, 'show', 'Value');
                axHandle1 = newplot(value(1));
                axHandle2 = newplot(value(2));
            else
                axHandle1 = newplot(subplot(1,2,1));
                axHandle2 = newplot(subplot(1,2,2));
            end
            
            % Preserve the hold state of axes
            holdState1 = ishold(axHandle1);
            holdState2 = ishold(axHandle2);
            
            % Prepare polar background for the histogram
            if max(obj.HistogramThresholds) == 0
                polarAxesLimit = 1;
            else
                polarAxesLimit = 4*max(obj.HistogramThresholds);
            end
            
            obj.preparePolarPlot(axHandle1, polarAxesLimit);
            
            polarAxesLimit = axHandle1.XLim(2);
            hold(axHandle1,'on');
            polarData = obj.PolarObstacleDensity;
            polarData(polarData > polarAxesLimit) = ...
                polarAxesLimit;
            
            polarData = [polarData; polarData];
            polarDataToPlot = polarData(:)';
            
            % Plot the polar obstacle density
            [x,y] = pol2cart(obj.AngularSectorEndPoints, polarDataToPlot);
            c = zeros(1, length(x));
            cnan = nan(1, length(x));
            xPts = [x;c;cnan];
            yPts = [y;c;cnan];
            plot(axHandle1, -yPts(:), xPts(:),'b-');
            
            % Connect polar lines
            plot(axHandle1, -y, x, 'b');
            
            % Draw circles representing the thresholds
            [xhigh, yhigh] = obj.circle(0,0,obj.HistogramThresholds(2));
            [xlow, ylow] = obj.circle(0,0,obj.HistogramThresholds(1));
            
            ptL = plot(axHandle1, xlow, ylow, 'm');
            plot(axHandle1, xhigh, yhigh, 'm');
            
            % Add legend and title to the first plot
            legend(axHandle1, ptL(1), message(...
                'robotics:robotalgs:vfh:HistThresholds').getString, ...
                'Location','northwest');
            title(axHandle1, ...
                message('robotics:robotalgs:vfh:PODTitle').getString);
            
            % Set the aspect ratio and reset the hold
            set(axHandle1, 'DataAspectRatio', [1, 1, 1]);
            axis(axHandle1, 'off');
            set(axHandle1, 'NextPlot', lower(get(axHandle1, 'NextPlot')));
            
            if ~holdState1
                hold(axHandle1, 'off');
            end
            
            set(get(axHandle1, 'XLabel'), 'Visible', 'on');
            set(get(axHandle1, 'YLabel'), 'Visible', 'on');
            
            % Create masked polar histogram with range sensor data
            
            % The maximum axis limit is slightly above active region
            axisLimit = obj.DistanceLimits(2) + 1;
            
            rangeIdx = obj.Ranges < axisLimit & ...
                (obj.Ranges >=obj.DistanceLimits(1)) & ...
                (obj.Ranges <=obj.DistanceLimits(2));
            
            % Prepare the polar plot with maximum axis limit
            obj.preparePolarPlot(axHandle2, axisLimit);
            hold(axHandle2,'on');
            
            % Plot the masked histogram
            maskedLength = 0.5*axisLimit;
            maskedHist = [obj.MaskedHistogram; obj.MaskedHistogram];
            maskedHistToPlot = maskedLength*maskedHist(:)';
            
            [xMask,yMask] = pol2cart(obj.AngularSectorEndPoints, ...
                maskedHistToPlot);
            cMask = zeros(1, length(xMask));
            cnanMask = nan(1, length(xMask));
            xPtsMask = [xMask;cMask;cnanMask];
            yPtsMask = [yMask;cMask;cnanMask];
            plot(axHandle2, -yPtsMask(:), xPtsMask(:),'b-');
            
            % Connect histogram lines on periphery
            plot(axHandle2, -yMask, xMask, 'b');
            
            % Percentage (of axisLimit) line lengths for directions
            tarLength = 0.5;
            
            % Plot target and last steering direction
            [xTd, yTd] = pol2cart(obj.TargetDirection, tarLength*axisLimit);
            [xPd, yPd] = pol2cart(obj.PreviousDirection, tarLength*axisLimit);
            pHTar = plot(axHandle2, -[yTd,0], [xTd,0], 'm', 'LineWidth', 2.0);
            pHPre = plot(axHandle2, -[yPd,0], [xPd,0], 'g--', 'LineWidth', 2.0);
            
            % Plot range readings
            [pX,pY] = pol2cart(obj.Angles(rangeIdx), obj.Ranges(rangeIdx));
            pHLaser = scatter(axHandle2, -pY, pX, 5, 'r', 'filled');
            
            % Plot the active region
            [xAct, yAct] = obj.circle(0,0,obj.DistanceLimits(2));
            pHAct = plot(axHandle2, -yAct, xAct, 'black');
            
            [xAct2, yAct2] = obj.circle(0,0,obj.DistanceLimits(1));
            plot(axHandle2, -yAct2, xAct2, 'black');
            
            % Get legend strings
            targetStr = ...
                message('robotics:robotalgs:vfh:TargetDir').getString;
            steerStr = ...
                message('robotics:robotalgs:vfh:SteeringDir').getString;
            rangeStr = ...
                message('robotics:robotalgs:vfh:RangeReadings').getString;
            distLimStr = ...
                message('robotics:robotalgs:vfh:DistanceLimits').getString;
            
            % Set legends and title
            if ~isempty(pX) && ~isnan(obj.PreviousDirection)
                % All children exists
                legend(axHandle2, [pHTar, pHPre, pHAct, pHLaser], ...
                    targetStr, steerStr, distLimStr, rangeStr, ...
                    'Location','northwest');
            elseif ~isempty(pX) && isnan(obj.PreviousDirection)
                % Previous direction is nan, so not plotted
                legend(axHandle2, [pHTar, pHAct, pHLaser], ...
                    targetStr, distLimStr, rangeStr, ...
                    'Location','northwest');
            else
                % If no laser points then previous direction cannot be nan
                legend(axHandle2, [pHTar, pHPre, pHAct], ...
                    targetStr, steerStr, distLimStr, 'Location','northwest');
            end
            
            title(axHandle2, ...
                message('robotics:robotalgs:vfh:MPHTitle').getString);
            
            % Set the aspect ratio and reset the hold
            set(axHandle2, 'DataAspectRatio', [1, 1, 1]);
            axis(axHandle2, 'off');
            set(axHandle2, 'NextPlot', lower(get(axHandle2, 'NextPlot')));
            
            if ~holdState2
                hold(axHandle2, 'off');
            end
            
            set(get(axHandle2, 'XLabel'), 'Visible', 'on');
            set(get(axHandle2, 'YLabel'), 'Visible', 'on');
            
            % Only return handle if user requested it.
            if nargout > 0
                ax = [axHandle1 axHandle2];
            end
        end
    end
    
    methods (Access = private)
        function buildPolarObstacleDensity(obj, ranges, angles)
            %buildPolarObstacleDensity Create polar obstacle density
            %   This function creates a polar obstacle density histogram
            %   from the range readings taking into account the robot
            %   radius and safety distance.
            
            idx = (ranges >=obj.DistanceLimits(1)) & ...
                (ranges <=obj.DistanceLimits(2));
            
            % Define variable length variables
            validRanges = ranges(idx);
            validAngles = angles(idx);
            % Forcing column-major dimension on these variables
            validRanges = validRanges(:);
            validAngles = validAngles(:);
            
            % Constants A and B used in Reference [1]
            constB = cast(1, 'like', ranges);
            constA = cast(obj.DistanceLimits(2), 'like', ranges);
            
            % Weighted ranges
            weightedRanges = constA - constB*validRanges;
            
            
            % If empty space in front of the robot
            if ~any(idx)
                obj.PolarObstacleDensity = zeros(1, obj.NumAngularSectors, 'like', ranges);
                return;
            end
            
            % Special case of one sector
            if obj.NumAngularSectors == 1
                validWeights = ones(1, numel(validRanges), 'like', ranges);
                obj.PolarObstacleDensity = (validWeights * weightedRanges)';
                return;
            end
            
            % If robot radius and safety distance both are zero, then use
            % primary histogram
            if obj.RobotRadius + obj.SafetyDistance == 0
                [~,bin] = histc(validAngles, obj.AngularSectorMidPoints);
                obstacleDensity = zeros(1, obj.NumAngularSectors, 'like', ranges);
                for i =1:length(bin)
                    obstacleDensity(1, bin(i)) = ...
                        obstacleDensity(1, bin(i)) + weightedRanges(i);
                end
                obj.PolarObstacleDensity = obstacleDensity;
                return;
            end
            
            % Equation (4) in Reference [1]
            % If the robot radius + safety distance is larger than the
            % Ranges, then "ASIN" will give complex values.
            
            % Using pi/2 as enlargement angle for ranges that are below
            % RobotRadius+SafetyDistance. In the original VFH algorithm
            % this case is not handled.
            sinOfEnlargement = cast(obj.RobotRadius + ...
                obj.SafetyDistance, 'like', ranges)./validRanges;
            
            % Using 1 - eps, which results in enlargement angles approximately
            % sqrt(eps) smaller than pi/2. This is required because floating point
            % errors can cause nondeterministic behavior in the downstream
            % computation at enlargement angle of pi/2.
            sinOfEnlargement(sinOfEnlargement >= 1) = 1 - eps(class(ranges));
            
            enlargementAngle = asin(sinOfEnlargement);
            
            % Polar obstacle density computation
            % Equation (5)-(6) in Reference [1]
            higherAng = validAngles + enlargementAngle;
            lowerAng  = validAngles - enlargementAngle;
            
            % Compute if a sector is within enlarged angle of a range
            % reading
            % If A X B, A X N and N X B have the same sign for Z-dimension,
            % then vector N is in between vectors A and B.
            
            % Create vectors for cross product computation
            lowerVec = [cos(lowerAng), sin(lowerAng), ...
                zeros(size(lowerAng, 1), 1, 'like', ranges)];
            higherVec = [cos(higherAng), sin(higherAng), ...
                zeros(size(higherAng, 1), 1, 'like', ranges)];
            validWeights = true(obj.NumAngularSectors, size(lowerVec,1));
            lh = cross(lowerVec, higherVec);
            kalpha = [cos(obj.AngularSectorMidPoints); ...
                sin(obj.AngularSectorMidPoints);...
                zeros(1,obj.NumAngularSectors, 'like', ranges)]';
            for i=1:obj.NumAngularSectors
                kalphaVec = repmat(kalpha(i,:), size(lowerVec,1), 1);
                lk = cross(lowerVec, kalphaVec);
                kh = cross(kalphaVec, higherVec);
                validWeights(i, :) = (abs(sign(lk(:,3))+sign(kh(:,3)) + ...
                    sign(lh(:,3))) > 1);
            end
            
            obj.PolarObstacleDensity = (validWeights * weightedRanges)';
        end
        
        function buildBinaryHistogram(obj)
            %buildBinaryHistogram Create binary histogram
            %   This function creates a binary polar histogram using the
            %   polar obstacle density. The function uses two threshold
            %   values to determine the binary values. The values falling
            %   in between the two threshold are chosen from binary
            %   histogram from the previous step.
            
            % Using thresholds, determine binary histogram
            % Equation (7) in Reference [1]
            % True means occupied sector
            obj.BinaryHistogram(obj.PolarObstacleDensity > ...
                obj.HistogramThresholds(1,2)) = true;
            obj.BinaryHistogram(obj.PolarObstacleDensity < ...
                obj.HistogramThresholds(1,1)) = false;
        end
        
        function buildMaskedPolarHistogram(obj, ranges, angles)
            %buildMaskedPolarHistogram Create masked histogram
            %   This function creates the masked polar histogram from the
            %   binary histogram. It considers the robot's turning radius
            %   and evaluates if the obstacles are too close restricting
            %   the robot movement towards certain direction.
            
            
            % Angle ahead =  0    rad
            % Angle left  =  pi/2 rad
            % Angle right = -pi/2 rad
            
            % Equation (8) in Reference [1]
            DXr = cast(0, 'like', ranges);
            DYr = cast(-obj.MinTurningRadius, 'like', ranges);
            DYl = cast(obj.MinTurningRadius, 'like', ranges);
            DXl = cast(0, 'like', ranges);
            
            % Only consider indices in active region
            % find function always returns double output, hence it is
            % required to cast it.
            idx = cast(find((ranges >=obj.DistanceLimits(1)) & ...
                (ranges <=obj.DistanceLimits(2))), 'like', ranges);
            
            % Equation (9) in Reference [1]
            DXj = ranges(idx).*cos(angles(idx));
            DYj = ranges(idx).*sin(angles(idx));
            
            distR = sqrt((DXr - DXj).^2 + (DYr - DYj).^2);
            distL = sqrt((DXl - DXj).^2 + (DYl - DYj).^2);
            
            % Equation (10a)-(10b) in Reference [1]
            blockedR = distR < (obj.MinTurningRadius + obj.RobotRadius + ...
                obj.SafetyDistance) & (angles(idx) <= 0);
            blockedL = distL < (obj.MinTurningRadius + obj.RobotRadius + ...
                obj.SafetyDistance) & (angles(idx) >= 0);
            
            % Compute limit angles
            phiR = angles(idx(find(blockedR, 1, 'last')));
            phiL = angles(idx(find(blockedL, 1 , 'first')));
            
            if isempty(phiR)
                phiR = obj.AngularSectorMidPoints(1, 1);
            elseif phiR(1,1) <= obj.AngularSectorMidPoints(1, 1)
                % Account for point inside first sector
                phiR = obj.AngularSectorMidPoints(1, 2);
            end
            
            if isempty(phiL)
                phiL = obj.AngularSectorMidPoints(1, end);
            elseif phiL(1,1) >= obj.AngularSectorMidPoints(1, end)
                % Account for point inside last sector
                phiL = obj.AngularSectorMidPoints(1, end-1);
            end
            
            % Equation (11) in Reference [1]
            occupiedAngularSectors = (obj.AngularSectorMidPoints < ...
                phiR(1,1)*ones(size(obj.AngularSectorMidPoints), 'like', ranges) | ...
                obj.AngularSectorMidPoints > ...
                phiL(1,1)*ones(size(obj.AngularSectorMidPoints), 'like', ranges));
            obj.MaskedHistogram = (obj.BinaryHistogram | occupiedAngularSectors);
        end
        
        function thetaSteer = selectHeadingDirection(obj, targetDir)
            %selectHeadingDirection Select heading direction
            %   This function selects the heading direction based on a
            %   target direction using a cost function. It first computes
            %   the candidate directions based on the empty sectors in the
            %   masked histogram and then selects one or two candidate
            %   directions for each sector.
            
            % Find open sectors
            changes = cast(diff([0 ~obj.MaskedHistogram 0]), 'like', targetDir);
            
            % Skip everything if there are no open sectors
            if ~any(changes)
                thetaSteer = cast(nan, 'like', targetDir);
                obj.PreviousDirection = thetaSteer;
                return;
            end
            
            foundSectors = cast(find(changes), 'like', targetDir);
            
            % Because masked histogram is binary, the foundSectors will
            % always have even elements.
            sectors = reshape(foundSectors, 2, []);
            sectors(2,1:end) = sectors(2,1:end) - ones(1, size(sectors, 2), 'like', targetDir);
            
            % Get size of different sectors
            angles = zeros(size(sectors), 'like', targetDir);
            angles(1, 1:end) = obj.AngularSectorMidPoints(sectors(1, 1:end));
            angles(2, 1:end) = obj.AngularSectorMidPoints(sectors(2, 1:end));
            
            sectorAngles = reshape(angles, 2, []);
            sectorSizes = cast(obj.AngularDifference.*diff(sectors, 1, 1), 'like', targetDir);
            
            % Compute one candidate direction for each narrow sector
            % Equation (12) in Reference [1]
            narrowIdx = sectorSizes < ...
                obj.NarrowOpeningThreshold*ones(size(sectorSizes), 'like', targetDir);
            narrowDirs = robotics.algs.internal.bisectAngles(...
                sectorAngles(1, narrowIdx), sectorAngles(2,narrowIdx));
            
            % Compute two candidates for each non-narrow sector
            % Equation (13) in Reference [1]
            nonNarrowDirs = [sectorAngles(1,~narrowIdx) + ...
                ones(size(sectorAngles(1,~narrowIdx)), 'like', targetDir)*...
                obj.NarrowOpeningThreshold/cast(2, 'like', targetDir), ...
                sectorAngles(2,~narrowIdx) - ...
                ones(size(sectorAngles(1,~narrowIdx)), 'like', targetDir)*...
                obj.NarrowOpeningThreshold/cast(2, 'like', targetDir)];
            
            % Add target, current and previous directions as candidates
            obj.TargetDirection = targetDir;
            currDir = cast(0, 'like', targetDir);
            if isnan(obj.PreviousDirection)
                obj.PreviousDirection = currDir;
            end
            
            % Final list of candidate directions
            % Equation (14) in Reference [1]
            candidateDirs = [nonNarrowDirs(1,:), ...
                narrowDirs(1,:), targetDir(1,1), currDir(1,1), ...
                obj.PreviousDirection(1,1)];
            
            % Remove occupied directions
            % If the candidate direction falls at the center of two bins
            % then check both the bins for occupancy
            tolerance = sqrt(eps(class(targetDir)));
            candToSectDiff = abs(bsxfun(@robotics.internal.angdiff, ...
                obj.AngularSectorMidPoints,candidateDirs.'));
            tempDiff = bsxfun(@minus, candToSectDiff, min(candToSectDiff,[],2));
            nearIdx = tempDiff < tolerance;
            
            freeDirs = true(1, size(nearIdx, 1));
            
            for i=1:length(freeDirs)
                freeDirs(i) = ~any(obj.MaskedHistogram(nearIdx(i,:)));
            end
            
            candidateDirections = candidateDirs(freeDirs);
            
            % Compute cost for each candidate direction
            % Equation (15) in Reference [1]
            costValues = obj.computeCost(candidateDirections, ...
                targetDir, currDir, obj.PreviousDirection);
            
            % Decide best direction to steer
            cVal = min(costValues);
            
            % Consider all costs that have very small difference to min
            % value
            cDiff = costValues - cVal;
            minCostIdx = cDiff < tolerance;
            
            thetaSteer = min(candidateDirections(minCostIdx));
            
            if isempty(thetaSteer)
                thetaSteer = cast(nan, 'like', targetDir);
            end
            obj.PreviousDirection = thetaSteer;
        end
        
        function cost = computeCost(obj, c, targetDir, currDir, prevDir)
            %computeCost Compute total cost using all cost components
            
            tdWeight = cast(obj.TargetDirectionWeight, 'like', targetDir);
            cdWeight = cast(obj.CurrentDirectionWeight, 'like', targetDir);
            pdWeight = cast(obj.PreviousDirectionWeight, 'like', targetDir);
            totalWeight = tdWeight + cdWeight + pdWeight;
            
            targetDir = targetDir*ones(1, numel(c), 'like', targetDir);
            currDir = currDir*ones(1, numel(c), 'like', targetDir);
            prevDir = prevDir*ones(1, numel(c), 'like', targetDir);
            
            cost =  (tdWeight*obj.localCost(c, targetDir) + ...
                cdWeight*obj.localCost(c, currDir) + ...
                pdWeight*obj.localCost(c, prevDir))./...
                3*totalWeight;
        end
        
        function cost = localCost(~, candidateDir, selectDir)
            %localCost Compute cost for each cost component
            
            % Cost computation for valid candidate indices
            cost = abs(robotics.internal.angdiff(candidateDir, selectDir));
        end
        
        function flag = isPrivateProperty(~, metaPropObj)
            %isPrivateProperty Check if a property is private
            propSetAccess = metaPropObj.SetAccess;
            
            cellCompare = false;
            if iscell(propSetAccess)
                for i = 1:length(propSetAccess)
                    cellCompare = cellCompare || isequal(propSetAccess{i},...
                        ?robotics.VectorFieldHistogram);
                end
            end
            
            flag = ((ischar(propSetAccess) && ...
                strcmp('private', propSetAccess)) || cellCompare) && ...
                ~strcmp(metaPropObj.Name, 'Description');
        end
    end
    
    methods (Static, Access = private)
        function preparePolarPlot(cax, maxrho)
            %preparePolarPlot Prepare polar background for polar plot
            
            hold_state = ishold(cax);
            % get x-axis text color so grid is in same color
            % get the axis gridColor
            axColor = get(cax, 'Color');
            gridAlpha = get(cax, 'GridAlpha');
            axGridColor = get(cax,'GridColor').*gridAlpha + ...
                axColor.*(1-gridAlpha);
            tc = axGridColor;
            ls = get(cax, 'GridLineStyle');
            
            % Hold on to current Text defaults, reset them to the
            % Axes' font attributes so tick marks use them.
            fAngle = get(cax, 'DefaultTextFontAngle');
            fName = get(cax, 'DefaultTextFontName');
            fSize = get(cax, 'DefaultTextFontSize');
            fWeight = get(cax, 'DefaultTextFontWeight');
            fUnits = get(cax, 'DefaultTextUnits');
            set(cax, ...
                'DefaultTextFontAngle', get(cax, 'FontAngle'), ...
                'DefaultTextFontName', get(cax, 'FontName'), ...
                'DefaultTextFontSize', get(cax, 'FontSize'), ...
                'DefaultTextFontWeight', get(cax, 'FontWeight'), ...
                'DefaultTextUnits', 'data');
            
            % only do grids if hold is off
            if ~hold_state
                % make a radial grid
                hold(cax, 'on');
                
                % ensure that Inf values don't enter into the limit calculation.
                hhh = line([-maxrho, -maxrho, maxrho, maxrho], ...
                    [-maxrho, maxrho, maxrho, -maxrho], 'Parent', cax);
                set(cax, 'DataAspectRatio', [1, 1, 1], ...
                    'PlotBoxAspectRatioMode', 'auto');
                v = [get(cax, 'XLim') get(cax, 'YLim')];
                ticks = sum(get(cax, 'YTick') >= 0);
                delete(hhh);
                % check radial limits and ticks
                rmin = 0;
                rmax = v(4);
                rticks = max(ticks - 1, 2);
                if rticks > 5   % see if we can reduce the number
                    if rem(rticks, 2) == 0
                        rticks = rticks / 2;
                    elseif rem(rticks, 3) == 0
                        rticks = rticks / 3;
                    end
                end
                
                % define a circle
                th = 0 : pi / 50 : 2 * pi;
                xunit = cos(th);
                yunit = sin(th);
                % now really force points on x/y axes to lie on them exactly
                inds = 1 : (length(th) - 1) / 4 : length(th);
                xunit(inds(2 : 2 : 4)) = zeros(2, 1);
                yunit(inds(1 : 2 : 5)) = zeros(3, 1);
                % plot background if necessary
                if ~ischar(get(cax, 'Color'))
                    patch('XData', xunit * rmax, 'YData', yunit * rmax, ...
                        'EdgeColor', tc, 'FaceColor', get(cax, 'Color'), ...
                        'HandleVisibility', 'off', 'Parent', cax);
                end
                
                % draw radial circles
                c82 = cos(82 * pi / 180);
                s82 = sin(82 * pi / 180);
                rinc = (rmax - rmin) / rticks;
                for i = (rmin + rinc) : rinc : rmax
                    hhh = line(xunit * i, yunit * i, 'LineStyle', ls, ...
                        'Color', tc, 'LineWidth', 1, ...
                        'HandleVisibility', 'off', 'Parent', cax);
                    text((i + rinc / 20) * c82, (i + rinc / 20) * s82, ...
                        ['  ' num2str(i)], 'VerticalAlignment', 'bottom', ...
                        'HandleVisibility', 'off', 'Parent', cax);
                end
                set(hhh, 'LineStyle', '-'); % Make outer circle solid
                
                
                % plot spokes
                th = (1 : 6) * 2 * pi / 12;
                cst = cos(th);
                snt = sin(th);
                cs = [-cst; cst];
                sn = [-snt; snt];
                line(rmax * cs, rmax * sn, 'LineStyle', ls, 'Color', tc, ...
                    'LineWidth', 1, 'HandleVisibility', 'off', 'Parent', cax);
                
                % annotate spokes in degrees
                rt = 1.1 * rmax;
                for i = 1 : length(th)
                    text(-rt * snt(i), rt * cst(i), int2str(i * 30),...
                        'HorizontalAlignment', 'center', ...
                        'HandleVisibility', 'off', 'Parent', cax);
                    if i == length(th)
                        loc = int2str(0);
                    else
                        loc = int2str(180 + i * 30);
                    end
                    text(rt * snt(i), -rt * cst(i), loc, 'HorizontalAlignment',...
                        'center', 'HandleVisibility', 'off', 'Parent', cax);
                end
                
                % set view to 2-D
                view(cax, 2);
                
                % set axis limits
                axis(cax, rmax * [-1, 1, -1.15, 1.15]);
            end
            
            % Reset defaults.
            set(cax, ...
                'DefaultTextFontAngle', fAngle , ...
                'DefaultTextFontName', fName , ...
                'DefaultTextFontSize', fSize, ...
                'DefaultTextFontWeight', fWeight, ...
                'DefaultTextUnits', fUnits );
        end
        
        function [xp, yp] = circle(x,y,r)
            %circle Used for drawing circles
            th = 0 : pi / 60 : 2 * pi;
            xp = r * cos(th) + x;
            yp = r * sin(th) + y;
        end
    end
    
end


function validateStepInputs(ranges, angles, target)
%validateStepInputs Validate inputs to step function

validateattributes(ranges, {'double', 'single'}, {'real', ...
    'vector', 'nonnegative', 'nonempty'}, 'step', 'ranges');

validateattributes(angles, {'double', 'single'}, {'nonnan', 'real', ...
    'vector', 'nonempty', 'finite'}, 'step', 'angles');

validateattributes(target, {'double', 'single'}, {'nonnan', 'real', ...
    'scalar', 'nonempty', 'finite'}, 'step', 'target direction');

isInvalidSize = (numel(ranges) ~= numel(angles));

coder.internal.errorIf(isInvalidSize, ...
    'robotics:robotalgs:vfh:InputSizeMismatch', 'ranges', 'angles');
end

function validateNonnegativeScalar(val, name)
%validateNonnegativeScalar Validate non-negative real scalar
validateattributes(val, {'double', 'single'}, {'nonnan', 'real', ...
    'scalar', 'nonnegative', 'finite'}, 'VectorFieldHistogram', name);
end

function validateNonnegativeArray(val, name)
%validateNonnegativeArray Validate non-negative two element array
validateattributes(val, {'double', 'single'}, {'nonnan', 'real', ...
    'numel', 2, 'finite', 'nonnegative'}, 'VectorFieldHistogram', name);
end
