classdef WCData < handle
    %WCData - Class for storing temporal variables used in the shared
    %control algorithms used with the PWC. 
    %   OBJ = WCData() The object keeps track of disagreement
    %   metrics between the autonomous planner and the user inputs.
    %
    %   WCData methods:
    %      clearData                - Clears stored data. Use for new goal.
    %      clearDir                 - Clears stored directions.
    %      addAngle                 - Adds new angle difference. 
    %      addDir                   - Adds a new direction
    %
    %   WCData properties:
    %      DataHistory              - History of disagreement metric
    %
    % Ariadna Estrada and Haoyu Yang 05/18/2018
    
    properties
        DataHistory = [];        % History of angular differences.
        averageValue = [];       % Average of angle differences. Low value means high agreement.
        directionArray = [];     % Joystick directions.
        stdDevValue = [];        % Standard deviation of current samples.
        MaxDataHistorySize = 100 %The maximum number of values that should be stored. FIFO.        
    end
    
    methods (Access = public)
        function obj = WCData()
            %WCData - Constructor 
        end
        
       
        function addAngle(obj, angle)
            %angleDifference - Keeps track of the latest angle differences 
                                 
            % Keep a history of previous robot positions (fixed-size FIFO)
            angleHistoryHandle = angle;
            addNewAngleHandle(obj, angleHistoryHandle);
            
            %Compute average of current set of angle differences
            obj.averageValue = sum(obj.DataHistory)/length(obj.DataHistory);
            obj.stdDevValue = std(obj.DataHistory);
        end
        
        function addDir(obj, dir)
            obj.directionArray = [obj.directionArray; dir];
        end
        
        function clearDir(obj)
           obj.directionArray = []; 
        end
        
        function clearData(obj)
            obj.DataHistory = [];
            obj.averageValue = [];
            obj.stdDevValue = [];
            obj.directionArray = [];
        end
        
    end
    
    methods (Access = protected)
        function addNewAngleHandle(obj, angleHandle)
            %addNewAngleHandle Store a new angle handle
            
            obj.DataHistory = [obj.DataHistory; angleHandle];
            if length(obj.DataHistory) > obj.MaxDataHistorySize
                % If we reached the maximum size of the array, delete the
                % oldest angle handle.
                obj.DataHistory(1) = [];
            end
        end
        
    end
    
end

