classdef WCVisualizer < handle
    %WCVisualizer - Class for plotting PWC and local environment
    %   OBJ = WCVisualizer(RANGES, MAP) creates a plot of the robot's world and
    %   according to the input axis ranges. The object keeps track of goal
    %   points, pose history, obstacle location data, and handles to graphical
    %   objects.
    %
    %   WCVisualizer methods:
    %      plotGoal                 - Plots the goal point on the graph
    %      plotPose                 - Plots the current robot pose
    %      plotData                 - Plots object location data and robot pose
    %      plotPlan                 - Plots current plan 
    %      plotFootprint            - Plots robot footprint
    %      plotStart                - Plots predefined mode starting point
    %
    %   WCVisualizer properties:
    %      FigureHandle             - Handle to the object figure
    %      AxesHandle               - Handle to the object axes
    %      Goal                     - Matrix of goal points
    %      PoseHandle               - Handle to the current pose object
    %      ArrowHandle              - Handle to current direction object
    %      PoseHistory              - Complete pose history matrix
    %      DataHistory              - Data of all occupied points
    %      Start                    - Matrix of starting points
    %      StartHandle              - Handle to current starting point
    %
    %   See also exampleHelperTurtleBotKeyboardControl, exampleHelperTurtleBotObstacleTimer
    
    %   Copyright 2014-2015 The MathWorks, Inc.
    %   Modified for PWC by Ariadna Estrada and Haoyu Yang 05/18/2018
    
    properties
        FigureHandle = [];      % Handle to the object figure
        AxesHandle = [];        % Handle to the object axes
        Goal = [];              % Matrix of goal points
        Start = [];             % Matrix of starting points
        StartHandle = [];       % Handle to current starting point
        GoalHandle = [];        % Handle to current goal object
        PoseHandle = [];        % Handle to the current pose object
        ArrowHandle = [];       % Handle to current direction object
        FootprintHandle = [];   % Handle to the current set of footprint points
        PoseHistory = [];       % History of all currently plotted robot poses
        DataHistory = [];       % History of all currently plotted laser scans
        PlanDataHistory = [];   % Autonomous planner current plan
        InterventionMode = []   % User selected intervention mode
    end
    
    properties (Constant, Access = private)
        %MaxPoseHistorySize The maximum number of poses that should be stored in PoseHistory
        %   PoseHistory is a FIFO and if it reaches MaxPoseHistorySize, the
        %   oldest element will be deleted.
        MaxPoseHistorySize = 1   
        
        %MaxDataHistorySize The maximum number of laser readings that should be stored in DataHistory
        %   DataHistory is a FIFO and if it reaches MaxDataHistorySize, the
        %   oldest element will be deleted.
        MaxDataHistorySize = 1
        
        %MaxPlanDataHistorySize The maximum number of plans that should be
        %stored. PlanDataHistory is a FIFO and if it reaches the
        %MaxPlanDataHistorySize, the oldest element will be deleted.
        MaxPlanDataHistorySize = 1;
    end
    
    methods (Access = public)
        function obj = WCVisualizer(ranges, map)
            %WCVisualizer - Constructor sets all the figure properties
            obj.FigureHandle = figure('Name','Robot Position','Position',[200,200,800,400],'CloseRequestFcn',@obj.closeFigure);
            obj.AxesHandle = axes('Parent',obj.FigureHandle,'XGrid','on','YGrid','on','XLimMode','manual','YLimMode','manual');
            axis(obj.AxesHandle,ranges);
            hold(obj.AxesHandle,'on');
            show(map, 'Parent', obj.AxesHandle);
            title('World map');
        end
        
        function plotGoal(obj,goal)
            %PLOTGOAL - Plots the goal point on the graph
            
            if ~ishandle(obj.FigureHandle)
                % Check that figure hasn't been closed
                error('Figure close request: Exiting');
            end
            delete(obj.GoalHandle);
            obj.GoalHandle = plot(obj.AxesHandle,goal(1),goal(2),'p','Color','b','MarkerFaceColor', 'b', 'MarkerSize',10);
            obj.Goal = [obj.Goal;goal];
        end
        
        function plotStart(obj,start)
            %PLOTStart - Plots the starting point on the graph
            
            if ~ishandle(obj.FigureHandle)
                % Check that figure hasn't been closed
                error('Figure close request: Exiting');
            end
            delete(obj.StartHandle);
            obj.StartHandle = plot(obj.AxesHandle,start(1),start(2),'p','Color','g','MarkerFaceColor', 'g', 'MarkerSize',10);
            obj.Start = [obj.Start;start];
        end
        
        function plotPose(obj,pose)
            %PLOTPOSE - Plots the current robot pose
            
            if ~ishandle(obj.FigureHandle)
                % Check that figure hasn't been closed
                error('Figure close request: Exiting');
            end
            
            % Delete current pose objects from plot
            delete(obj.PoseHandle);
            delete(obj.ArrowHandle);
            
            % Plot current pose and direction
            obj.PoseHandle = plot(obj.AxesHandle, pose(1),pose(2),'o','MarkerSize',5);
            obj.ArrowHandle = plot(obj.AxesHandle,[pose(1), pose(1) + 0.5*cos(pose(3))], ...
                [pose(2), pose(2) + 0.5*sin(pose(3))], ...
                '*','MarkerSize',2,'Color','r','LineStyle','-');

            % Keep a history of previous robot positions (fixed-size FIFO)
            poseHistoryHandle = plot(obj.AxesHandle,pose(1),pose(2),'*','Color','c','MarkerSize',2);
            addNewPoseHandle(obj, poseHistoryHandle);
        end
        
        function plotData(obj,pose,data)
            %PLOTDATA - Plots object location data and robot pose
            
            if ~ishandle(obj.FigureHandle)
                % Check that figure hasn't been closed
                error('Figure close request: Exiting');
            end
            
            % Plot pose
            plotPose(obj,pose);
            
            th = pose(3)-pi/2;
            
            if ~isempty(data)
                % Compute the world-frame location of laser points
                dataWorld = data*[cos(th) sin(th);-sin(th) cos(th)] ...
                    + repmat(pose(1:2),[numel(data(:,1)),1]);

                % Plot the transformed laser data on the world map
                % Also keep a history of previous laser data handles (fixed-size FIFO)
                laserDataHandle = plot(obj.AxesHandle,dataWorld(:,1), dataWorld(:,2), '*', 'MarkerSize',1,'Color','r');
                addNewLaserDataHandle(obj, laserDataHandle);
            end
        end
        
        function plotPlan(obj, waypoints)
            planDataHandle =  plot(obj.AxesHandle, waypoints(:,1), waypoints(:,2), '-',  'MarkerSize', 2, 'Color', 'g');
            addNewPlanDataHandle(obj, planDataHandle);
        end
        
        function plotFootprint(obj, footprint)
            delete(obj.FootprintHandle);
            footprint=[footprint;footprint(1,:)];
            obj.FootprintHandle = plot(obj.AxesHandle, footprint(:,1), footprint(:,2), 'b-');

        end
    end
    
    methods (Access = protected)
        function addNewPoseHandle(obj, poseHandle)
            %addNewPoseHandle Store a new pose graphics handle
            
            obj.PoseHistory = [obj.PoseHistory; poseHandle];
            if length(obj.PoseHistory) > obj.MaxPoseHistorySize
                % If we reached the maximum size of the array, delete the
                % oldest graphics handle.
                delete(obj.PoseHistory(1));
                obj.PoseHistory(1) = [];
            end
        end
        
        function addNewLaserDataHandle(obj, laserDataHandle)
            %addNewLaserDataHandle Store a new laser data graphics handle
            
            obj.DataHistory = [obj.DataHistory; laserDataHandle];
            if length(obj.DataHistory) > obj.MaxDataHistorySize
                % If we reached the maximum size of the array, delete the
                % oldest graphics handle.
                delete(obj.DataHistory(1));
                obj.DataHistory(1) = [];
            end
        end
        
        function addNewPlanDataHandle(obj, planDataHandle)
            %addNewPlanDataHandle Store a new plan data graphics handle
            
            obj.PlanDataHistory = [obj.PlanDataHistory;planDataHandle];
            if length(obj.PlanDataHistory) > obj.MaxPlanDataHistorySize
                delete(obj.PlanDataHistory(1));
                obj.PlanDataHistory(1) = [];
            end
            
        end
        
        function closeFigure(obj,~,~)
            %CLOSEFIGURE - Callback function that deletes the figure
            % handle
            
            delete(obj.FigureHandle);
        end
    end
    
end

