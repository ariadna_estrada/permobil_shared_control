function in_collision = checkCollision( u , dt, collisionThresh)
%checkCollision Forward projects the user input u by time dt using a non-holonomic
%kinematic model and evaluates the occupancy of the resulting pose in a local costmap. If
%the cell value is higher than a threshold then the in_collision will be true.

global costmap
global footprintData

if (nargin < 2)
    dt = 2; 
end
if (nargin < 3)
    collisionThresh = 0.7;
end

legs_footprint = footprintData(3:4,:);
back_footprint = footprintData(7:8,:);

%If robot is moving forward, predict position of front of the robot. Otherwise, predict
%pose of back.
if u(1) >= 0
    next_pose = sample_motion_nonholonomic(u, legs_footprint, dt);
else
    next_pose = sample_motion_nonholonomic(u, back_footprint, dt);
end

try
    prob_collision = getOccupancy(costmap, next_pose(:,1:2));
catch me
    warning(me.message);
    in_collision = 0;
    return
end

%Set collision prediction to true if the projected pose lands on a cell with an occupancy
%value higher than a threshold.
in_collision = any(prob_collision > collisionThresh);


end

