function checkPlannerStatus(handles, diffGoals)
%Check whether the current goal has been aborted. If it has, re-send it to
%the planner. Re-send goal in case planner goal has been reached but is
%different from the user's goal. 

planner_status = handles.substatus.LatestMessage.StatusList.Status;
if (planner_status == 4)
    disp('resending goal to planner')
    sendGoal(handles.actionClient, handles.msgGoal);
elseif (diffGoals && planner_status == 3)
    disp('reached planner goal; resending goal to planner')
    sendGoal(handles.actionClient, handles.msgGoal);
end 

end