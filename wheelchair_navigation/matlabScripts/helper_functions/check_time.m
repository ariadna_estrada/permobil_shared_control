function boo = check_time(t1, t2, threshold)
% check_time(t1, t2, threshold)
% helper function to check if the difference between time stamp t2 
% and time stamp t1 is smaller than a threshold
% t1 & t2: clock array of size 1*6
% threshold: an integer that stands for seconds, ranging [0 60]
% boo: a return boolean value

n = 6;
% threshold is set to have range [0 60]
threshold = min(60, abs(threshold));
for i = 1:n-1
    % If any column before {seconds} is greater then return false
    if(t2(i) > t1(i))
        boo = false;
        return
    end
end

% Compute the difference in seconds
if(t2(6) - t1(6) < threshold)
    boo = true;
else
    boo = false;
end

end