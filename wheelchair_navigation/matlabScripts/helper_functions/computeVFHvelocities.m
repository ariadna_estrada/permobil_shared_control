function [u_free] = computeVFHvelocities(vfh, u, laserData)
%[u_free] = computeVFHvelocities(vfh, u, laserData)
% vfh: Vector field histogram object u: control input vector [v w]
% laserData: struct with fields Ranges and Angles
persistent angPID

%Initialize PID Controller
if isempty(angPID)
    angGains = struct('pgain',1.5,'dgain',-3,'igain',0,'maxwindup',0','setpoint',0);
    angPID = ExampleHelperPIDControl(angGains);
end

%Parameters
maxWController = 1; %The controller cap is high to react quickly to collisions.
minWController = 0.5; %The wheelchair does not move below this angluar speed

uAngle = atan2(u(2),u(1));
vfhAngle = step(vfh, laserData.Ranges, laserData.Angles, uAngle);

if isnan(vfhAngle) 
    u_free = [0, u(2)];
else
    angleDiff = abs( angdiff(uAngle, vfhAngle) );
    if angleDiff > pi/4
        u_free(1) = u(1)*exp(-0.4*angleDiff);
    else
        u_free(1) = u(1);
    end
    
    u_free(2) = update(angPID, -vfhAngle);
    if (abs(u_free(2)) > maxWController)
        u_free(2) = maxWController*sign(u_free(2));
    end

end