image = imread('lab_20180802.pgm');
% figure
% imshow(image)
imageBW = image < 100;
% figure
% imshow(imageBW)
map = robotics.BinaryOccupancyGrid(imageBW,20);
figure
show(map)

%--------------------------------------
% res = 20; %20 px per meter
% [n,d] = size(image);
% rem_left = 1;
% rem_right = 1;
% rem_top = 2;
% rem_bottom = 0.5;
% 
% crop_im = image(rem_top*res+1:n-rem_bottom*res, rem_left*res+1:d-rem_right*res);
% %crop_im = image(50+1:416-45,:);
% imshow(crop_im)
% imageBW = crop_im < 100;
% imshow(imageBW)
% map = robotics.BinaryOccupancyGrid(imageBW,res);
% figure
% show(map)
map.GridLocationInWorld = [-6.025000000000000,-4.255000000000000];
save('lab_20180802.mat', 'map');
%imwrite(crop_im, 'lab_20180724.pgm');

