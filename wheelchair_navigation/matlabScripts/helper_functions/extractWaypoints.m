function [ waypoints , pathLength] = extractWaypoints ( pathMsg )
%extractWaypoints ( pathMsg )
%   Returns path points from a path message type. Also returns the length
%   of the plan. 
if ~isempty(pathMsg)
    n = length(pathMsg.Poses);
    if n > 10
        pathMsg.Poses = pathMsg.Poses(1:10:end);
        n = length(pathMsg.Poses);
    end
    waypoints = zeros(n,2);
    for i = 1:n
        point = pathMsg.Poses(i,1).Pose.Position;
        waypoints(i,1) = point.X;
        waypoints(i,2) = point.Y;
    end

    ds = diff(waypoints);
    pathLength = sum(sqrt(sum(ds.^2, 2))); 

else
    waypoints = [];
    pathLength = inf;
end

end

