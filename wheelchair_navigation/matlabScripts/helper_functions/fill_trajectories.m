% g1 = [8.7, 15.5];
% g2 = [11, 10];
% g3 = [5.3, 14.85];
% g4 = [1.9, 11.3];
% g5 = [11, 0.75];
% g6 = [3.8, 5.2];
% g7 = [-0.5, 6];
% g8 = [-2.1, 0];
g1 = [9.1, 14.55];
g2 = [5.6, 14.75];
g3 = [10.7, 10.3];
g4 = [5.25, 10.6];
g5 = [11, 0.75];
g6 = [4, 5.2];
g7 = [-0.5, 6];
g8 = [-1, 0.1];
g =[g1;g2;g3;g4;g5;g6;g7;g8];

modes = {'Dynamic_A', 'Efficiency_A', 'Disagreement_A', 'HighLevel_A', 'Woz_A', 'CollisionAvoidance_A'};
modes2 = {'CollisionAvoidance_A', 'AudioPrompt_A', 'AwayFromGoal_A', 'Simple_A'};

startA = [1, 6, 4;...
          2, 7, 3;...
          3, 8, 4;...
          4, 7, 1;...
          2, 5, 1;...
          3, 6, 2];
startB = [5, 2, 7;...
          6, 1, 5;...
          7, 4, 6;...
          8, 3, 7;... 
          7, 3, 8;...
          8, 2, 5];
%%
n = 6;
for r=1:n
    mode = modes{1,r}

    indA = startA(r,:);
    indB = startB(r,:);

    trajectories.(mode).room_A.start     = g(indA(1),:);
    trajectories.(mode).room_A.end       = g(indA(2),:);
    trajectories.(mode).room_A.planner   = g(indA(2),:);
    trajectories.(mode).room_A.end_2     = g(indA(3),:);
    trajectories.(mode).room_A.planner_2 = g(indA(3),:);

    trajectories.(mode).room_B.start     = g(indB(1),:);
    trajectories.(mode).room_B.end       = g(indB(2),:);
    trajectories.(mode).room_B.planner   = g(indB(2),:);
    trajectories.(mode).room_B.end_2     = g(indB(3),:);
    trajectories.(mode).room_B.planner_2 = g(indB(3),:);
end

%%
modes = {'Dynamic_B', 'Efficiency_B', 'Disagreement_B', 'HighLevel_B', 'Woz_B' , 'CollisionAvoidance_A'};
% modes2 = {'CollisionAvoidance_B', 'AudioPrompt_B', 'AwayFromGoal_B', 'Simple_B'};


startB = [5, 4, 2, 8, 7; 6, 3, 1, 7, 8; 7, 2, 4, 6, 8;...
          8, 1, 3, 5, 6; 8, 4, 2, 7, 8; 7, 1, 3, 5, 6];
startA = [1, 6, 8, 4, 2; 2, 7, 8, 3, 1; 3, 5, 6, 2, 4;...
          4, 8, 7, 1, 3; 1, 6, 8, 4, 2; 2, 8, 7, 3, 1];
%%
r = 5;
mode = modes{1,r};

indA = startA(r,:);
indB = startB(r,:);

trajectories.(mode).room_A.start     = g(indA(1),:);
trajectories.(mode).room_A.end       = g(indA(2),:);
trajectories.(mode).room_A.planner   = g(indA(3),:);
trajectories.(mode).room_A.end_2     = g(indA(4),:);
trajectories.(mode).room_A.planner_2 = g(indA(5),:);

trajectories.(mode).room_B.start     = g(indB(1),:);
trajectories.(mode).room_B.end       = g(indB(2),:);
trajectories.(mode).room_B.planner   = g(indB(3),:);
trajectories.(mode).room_B.end_2     = g(indB(4),:);
trajectories.(mode).room_B.planner_2 = g(indB(5),:);

% save('trajectories.mat', 'trajectories');