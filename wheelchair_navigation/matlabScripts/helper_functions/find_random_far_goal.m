function goal = find_random_far_goal(goals, current_pos, d)
%find_random_far_goal - given a list of goals, find a random far goal that
%is at least d meters far from the current location.
%goals: a list of 2d goals
%current_pose: current position in the map
%d: a minimum threshold to indicate selection radius
nGoals = length(goals);
indices = zeros(nGoals,1);
for i=1:nGoals
    distance = norm(goals(i)-current_pos, 2);
    if(distance >= d)
        indices(i) = i;
    end
end
%Remove zeros from indices array
indices = indices(indices~=0);
goal = goals(indices(randi(length(indices))),:);

end