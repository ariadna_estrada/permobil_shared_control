function [ obstacle ] = getCostmap( costmapMsg )
%[ obstacle ] = getCostmap( costmapMsg ) 
%   Input: 
%       -costmapMsg: 1x1 OccupancyGrid message from costmap subscriber
%   Output: 
%       -obstacle: 1x1 struct with fields needed to compute value function.

    obstacle.resolution = double(costmapMsg.Info.Resolution);
    obstacle.width = double(costmapMsg.Info.Width);
    obstacle.height = double(costmapMsg.Info.Height);
    obstacle.origin = [costmapMsg.Info.Origin.Position.X, ...
                       costmapMsg.Info.Origin.Position.Y];
    obstacle.cost = costmapMsg.Data;
    obstacle.cost = double(max(0,min(100,obstacle.cost)));
    obstacle.cost = reshape(obstacle.cost, obstacle.width, obstacle.height);

end
