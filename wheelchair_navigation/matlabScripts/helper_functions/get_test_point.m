function testPoint = get_test_point( pathMsg )
%get_test_point: extract the last point from planner's local plan
%testPoint: vector size 2. [x, y] coordinates of the last point in the local plan. 
    point = pathMsg.Poses(end,1).Pose.Position;
    testPoint(1) = point.X;
    testPoint(2) = point.Y;
end