%Script to retreive costmap from ROS and store it as an occupancy grid
global global_costmap

clear;clc;

%Get costmap from ROS. Skip section if loading a saved costmap.
rosshutdown;
rosinit('localhost');
costmapSub = rossubscriber('/move_base/global_costmap/costmap');
costmapMSG = costmapSub.LatestMessage;
obstacle = getCostmap(costmapMSG);
imagesc(obstacle.cost);

norm_cost = double(obstacle.cost) ./100;
map_cost = imrotate(norm_cost, 90);
global_costmap = robotics.OccupancyGrid(map_cost, 20); %resolution is 20 cells / meter
global_costmap.GridLocationInWorld = [-3.525, -10.025];

%update gridLocationInWorld manually

save('lab209_global_costmap.mat', 'global_costmap');