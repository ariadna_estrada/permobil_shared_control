function player = prompt_player(mode)
    switch mode
        case 'ttr'
            [ttr, ttr_Fs] = audioread('try_turning_right.mp3');
            player = audioplayer(ttr, ttr_Fs);
        case 'ttl'
            [ttl, ttl_Fs] = audioread('try_turning_left.mp3');
            player = audioplayer(ttl, ttl_Fs);
        case 'goal'
            [goal, goal_Fs] = audioread('goal.mp3');
            player = audioplayer(goal, goal_Fs);
        case 'avoidance'
            [av, av_Fs] = audioread('avoiding_collision.mp3');
            player = audioplayer(av, av_Fs);
        otherwise
            disp('Mode not found');
    end
end