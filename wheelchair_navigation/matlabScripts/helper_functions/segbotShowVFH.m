function segbotShowVFH(vfh)
    %segbotShowVFH(vfh) - Display steps of VFH+ algorithm
    %   segbotShowVFH(vfh) - Takes a vector field histogram object and
    %   displays last step of vector field histogram.
    
    persistent ax 
    
    if isempty(vfh) 
        return;
    end
    
   
    figName = 'VFH';
    fig = findall(0,'Type','Figure','Name',figName);
    if isempty(fig)
        % Create the figure 
        fig = figure('Name','VFH');
    end
    
    if isempty(ax)
        try
            ax = show(vfh);
        catch
            ax = [];
            return
        end
    else
        try
%             axes(ax);
            show(vfh, 'Parent', ax);
        catch
            ax = [];
            disp('cannot display vfh')
            return
        end
    end
end

