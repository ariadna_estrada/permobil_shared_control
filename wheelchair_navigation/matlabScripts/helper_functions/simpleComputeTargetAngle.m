function [ targetAngleRobot ] = simpleComputeTargetAngle( g, p )
% Determine angle to get to goal directly
% Inputs:
% - g: vector array size n x 2. Array of goal locations. [x1, y1; xn, yn]
% - p: vector size 3. Vector with robot pose [x y theta]
% Outputs:
% - targetAngleRobot: vector size n. Angle in radians [-pi, pi] between
% the current robot orientation and the goal locations in g.

targetPoint = g - p(1:2);
targetAngleGlobal = atan2(targetPoint(:,2),targetPoint(:,1));
targetAngleRobot = targetAngleGlobal - p(3);
targetAngleRobot = robotics.internal.wrapToPi(targetAngleRobot);

end

