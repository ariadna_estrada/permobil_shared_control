clear;close all;
%% To test lasers run in terminal
% 1. roslaunch JoyControl wheelchair_shared_control.launch
% 2. roslaunch wheelchair_navigation wheelchair_eband.launch
% 3. roslaunch ira_laser_tools wheelchair_scan_merger.launchCartesian

rosshutdown;
rosinit('localhost');

%% Global variables
global laserData

 %% Create ROS publisher for sending out velocity commands
[vel_wc.pub, vel_wc.msg] = rospublisher('/cmd_vel', 'JoyControl/joystick');

 %% Subscribe to ROS topics
sub_user_joy = rossubscriber('/joy_user', {@joyUserCallbackFcn, vel_wc});
sub_lasers = rossubscriber('/scan_multi', {@laserCallbackFcn});


