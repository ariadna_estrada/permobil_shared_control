clear;close all;
%% To test connection run in terminal
% roslaunch JoyControl wheelchair_shared_control.launch

rosshutdown;
rosinit('localhost');


% Create ROS publishers
[odom.pub, odom.msg] = rospublisher('/hector_odom', 'nav_msgs/Odometry');
[vel_wc.pub, vel_wc.msg] = rospublisher('/cmd_vel', 'JoyControl/joystick');

% Subscribe to ROS topics
sub_scanmatch = rossubscriber('/scanmatch_odom',  {@odomCallbackFcn, odom});
sub_user_joy = rossubscriber('/joy_user', {@joyUserCallbackFcn, vel_wc});