classdef wcStates
    %wcStates - Enumerated class with no methods. Contains
    %   enumerated list of wheelchair states
       
    enumeration
        a0, a1
    end
    
end