function userEfficiency = weightUserInputs(joystickDir, n, t, e)
% weightUserInputs(joystickDir, n, t, e)
% joystickDir: an array of user's joystick input directions, size no lager
% than 10
% n: number of inputs used for user insistance/frustration testing
% t: threshold on angular difference between a given pair of user input
% directions
% e: the current user efficiency
% This funcion takes a series of size n of user input directions and evaluate the
% angular difference between n-1 consecutive pairs of directions, such
% evaluated results are stored in another array D, if max(D) <= t, then
% there is indication of user's insistance/frustration, an incresed
% userEfficiency/inputWeight will be assigned

    if(n > 10)
        n = 10;
    end

    if(length(joystickDir) < n)
        n = length(joystickDir);
    end
    
    D = zeros(1,n);
    
    for i = 2:n
        D(i) = abs(angdiff(joystickDir(i-1),joystickDir(i)));
    end
    
    if(max(D) <= t)
        userEfficiency = e + 0.1;
    else
        userEfficiency = e;
    end


end