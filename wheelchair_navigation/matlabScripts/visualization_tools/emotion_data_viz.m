function varargout = emotion_data_viz(varargin)
% EMOTION_DATA_VIZ MATLAB code for emotion_data_viz.fig
%      EMOTION_DATA_VIZ, by itself, creates a new EMOTION_DATA_VIZ or raises the existing
%      singleton*.
%
%      H = EMOTION_DATA_VIZ returns the handle to a new EMOTION_DATA_VIZ or the handle to
%      the existing singleton*.
%
%      EMOTION_DATA_VIZ('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EMOTION_DATA_VIZ.M with the given input arguments.
%
%      EMOTION_DATA_VIZ('Property','Value',...) creates a new EMOTION_DATA_VIZ or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before emotion_data_viz_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to emotion_data_viz_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help emotion_data_viz

% Last Modified by GUIDE v2.5 29-Aug-2018 11:39:12

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @emotion_data_viz_OpeningFcn, ...
                   'gui_OutputFcn',  @emotion_data_viz_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before emotion_data_viz is made visible.
function emotion_data_viz_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to emotion_data_viz (see VARARGIN)

% Choose default command line output for emotion_data_viz
handles.output = hObject;


emotion_data = varargin{1};
trial_data = varargin{2};
trial_directory = varargin{3};
map = varargin{4};

emotions = {'Joy', 'Fear', 'Disgust', 'Sadness', 'Anger', 'Surprise', 'Contempt', 'Valence'};

%Define some colors
my_blue = [0 0.4470 0.7410];
my_red = [0.85 0.325 0.098];
my_yellow = [0.929 0.694 0.125];


%Show first image
img_number = 0;
img_path = fullfile(trial_directory, 'images/',['frame' sprintf('%04d',img_number) '.jpg']);
img_rgb = imread(img_path);
imshow(img_rgb, 'Parent', handles.axes1);

%Plot discrete emotions data
set(handles.axes2, 'nextPlot', 'add', 'XLim', [0 emotion_data(end,1)],'box', 'on');
plot(handles.axes2, emotion_data(:,1), emotion_data(:,2:9), 'LineWidth', 2);
grid(handles.axes2, 'on'); 
ylabel(handles.axes2, 'Emotion Level');
legend(handles.axes2, emotions{1:8}, 'Location', 'northeastoutside');

%Plot weights
set(handles.axes3, 'nextPlot', 'add', 'XLim', [0 trial_data.cmd_vel(end,1)], 'YLim', [-0.2 1.2], 'box', 'on');
plot(handles.axes3, trial_data.cmd_vel(:,1), trial_data.user_weight, 'LineWidth', 2);
grid(handles.axes3, 'on'); 
ylabel(handles.axes3, 'User autonomy');
legend(handles.axes3, '     user \alpha', 'Location', 'northeastoutside');

%Plot disagreement
set(handles.axes4, 'nextPlot', 'add', 'XLim', [0  trial_data.u_joy(end,1)], 'YLim', [0 pi], 'box', 'on');
plot(handles.axes4, trial_data.u_joy(:,1), trial_data.disagreement, 'LineWidth', 2);
grid(handles.axes4, 'on'); 
ylabel(handles.axes4, 'disagreement [rad]');
legend(handles.axes4, '|\theta_u - \theta_r|', 'Location', 'northeastoutside');


%Plot change in magnitude of commanded velocity
set(handles.axes5, 'XLim', [0, trial_data.u_joy(end,1)], 'box', 'on');
hold(handles.axes5, 'on');

plot(handles.axes5, trial_data.u_joy(:,1), trial_data.mag_change, 'LineWidth', 2, 'LineStyle', '-', 'Color', my_red);
grid(handles.axes5, 'on'); 
ylabel(handles.axes5, '|u_{shared}| / |u_{user}|');
legend(handles.axes5, '\Delta mag','Location', 'northeastoutside');

% %Plot linear velocities
% set(handles.axes5, 'XLim', [0, trial_data.joy_user(end,1)], 'YLim', [-1.2 1.2], 'box', 'on');
% hold(handles.axes5, 'on');
% 
% plot(handles.axes5, trial_data.joy_user(:,1), trial_data.joy_user(:,2), 'LineWidth', 2, 'LineStyle', ':', 'Color', my_red);
% plot(handles.axes5, trial_data.eband_vel(:,1), trial_data.eband_vel(:,2), 'LineWidth', 2, 'LineStyle', ':',  'Color', my_yellow);
% plot(handles.axes5, trial_data.cmd_vel(:,1), trial_data.cmd_vel(:,2), 'LineWidth', 2,  'LineStyle', '-', 'Color', my_blue);
% 
% grid(handles.axes5, 'on'); 
% ylabel(handles.axes5, '$v$', 'Interpreter','latex');
% legend(handles.axes5, '    v_{user}', '    v_{robot}', 'v_{shared}','Location', 'northeastoutside');

%Show map
hold(handles.axes6,'on');
show(map, 'Parent', handles.axes6); title(handles.axes6, '');
xlabel(handles.axes6, 'x [m]'); ylabel(handles.axes6, 'y [m]');
           

% Plot current pose and laser readings
handles.trial_data = trial_data;

plotMapData(0, handles);

%
handles.trial_directory = trial_directory;


% Update handles structure
guidata(hObject, handles);

% UIWAIT makes emotion_data_viz wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = emotion_data_viz_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
value = get(hObject,'Value');
img_number = floor(value);

img_path = fullfile(handles.trial_directory, 'images/',['frame' sprintf('%04d',img_number) '.jpg']);
img_rgb = imread(img_path);
imshow(img_rgb, 'Parent', handles.axes1);

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on mouse press over axes background.
function axes2_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axes2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

value = hObject.CurrentPoint(1,1);
[~, img_number] = min( abs(value - handles.trial_data.img_time) );
t = handles.trial_data.img_time(img_number)
img_path = fullfile(handles.trial_directory, 'images/',['frame' sprintf('%04d',img_number) '.jpg']);
img_rgb = imread(img_path);
imshow(img_rgb, 'Parent', handles.axes1);

% Plot current pose and laser readings
plotMapData(value, handles);

% --- Executes on mouse press over axes background.
function axes3_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axes3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = hObject.CurrentPoint(1,1);
[~, img_number] = min( abs(value - handles.trial_data.img_time) );
t = handles.trial_data.img_time(img_number)
img_path = fullfile(handles.trial_directory, 'images/',['frame' sprintf('%04d',img_number) '.jpg']);
img_rgb = imread(img_path);
imshow(img_rgb, 'Parent', handles.axes1);

% Plot current pose and laser readings
plotMapData(value, handles);

% --- Executes on mouse press over axes background.
function axes4_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axes4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = hObject.CurrentPoint(1,1);
[~, img_number] = min( abs(value - handles.trial_data.img_time) );
t = handles.trial_data.img_time(img_number)
img_path = fullfile(handles.trial_directory, 'images/',['frame' sprintf('%04d',img_number) '.jpg']);
img_rgb = imread(img_path);
imshow(img_rgb, 'Parent', handles.axes1);

% Plot current pose and laser readings
plotMapData(value, handles);

% --- Executes on mouse press over axes background.
function axes5_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axes5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = hObject.CurrentPoint(1,1);
[~, img_number] = min( abs(value - handles.trial_data.img_time) );
t = handles.trial_data.img_time(img_number)
img_path = fullfile(handles.trial_directory, 'images/',['frame' sprintf('%04d',img_number) '.jpg']);
img_rgb = imread(img_path);
imshow(img_rgb, 'Parent', handles.axes1);

plotMapData(value, handles);


function plotMapData(time,  handles)
% Delete current pose/laser/goal objects from plot
delete(handles.axes6.Children(1:end-1));

%find pose that was published closest to the 'time' clicked
[~, pose_number] = min( abs(time - handles.trial_data.amcl_pose(:,1)));
pose = handles.trial_data.amcl_pose(pose_number,2:4);
handles.PoseHandle = plot(handles.axes6, pose(1),pose(2),'o','MarkerSize',5, 'Color', 'b');
handles.ArrowHandle = plot(handles.axes6,[pose(1), pose(1) + 0.5*cos(pose(3))], ...
    [pose(2), pose(2) + 0.5*sin(pose(3))], ...
    '*','MarkerSize',2,'Color','r','LineStyle','-');

%find laser msg published closest to 'time' clicked
[~, laser_number] = min( abs(time - handles.trial_data.laser.time));
laserMsg = handles.trial_data.laser.msgs{laser_number,1};
cartesian = readCartesian(laserMsg) * [0 1; -1 0];
th = pose(3)-pi/2;
            

% Compute the world-frame location of laser points
dataWorld = cartesian*[cos(th) sin(th);-sin(th) cos(th)] ...
    + repmat(pose(1:2),[numel(cartesian(:,1)),1]);

% Plot the transformed laser data on the world map
handles.laserHandle = plot(handles.axes6,dataWorld(:,1), dataWorld(:,2), '*', 'MarkerSize',0.05,'Color','g');

%find goal location at that 'time'
[~, goal_number] = find( handles.trial_data.goal(:,1) <= time, 1, 'last' );
goal = handles.trial_data.goal(goal_number,2:3);
% handles.goalHandle = plot(handles.axes6, goal(1), goal(2), 'p', 'Color', 'b', 'MarkerFaceColor', 'b');


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2


% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3
