function varargout = goal_planning_viz(varargin)
% GOAL_PLANNING_VIZ MATLAB code for goal_planning_viz.fig
%      GOAL_PLANNING_VIZ, by itself, creates a new GOAL_PLANNING_VIZ or raises the existing
%      singleton*.
%
%      H = GOAL_PLANNING_VIZ returns the handle to a new GOAL_PLANNING_VIZ or the handle to
%      the existing singleton*.
%
%      GOAL_PLANNING_VIZ('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GOAL_PLANNING_VIZ.M with the given input arguments.
%
%      GOAL_PLANNING_VIZ('Property','Value',...) creates a new GOAL_PLANNING_VIZ or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before goal_planning_viz_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to goal_planning_viz_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help goal_planning_viz

% Last Modified by GUIDE v2.5 27-Jun-2018 15:59:50

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @goal_planning_viz_OpeningFcn, ...
                   'gui_OutputFcn',  @goal_planning_viz_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before goal_planning_viz is made visible.
function goal_planning_viz_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to goal_planning_viz (see VARARGIN)
l1 = load('lab1_goals.mat','lab1_goals');
l2 = load('lab2_goals.mat','lab2_goals');
map = load('lab_20180725.mat','map');

% Choose default command line output for goal_planning_viz
handles.output = hObject;

% Plot lab map
show(map.map)
hold on
% Create labels for goals in the first lab
n = size(l1.lab1_goals,1);
labels = string(zeros(n,1));
for i = 1:n
    labels(i) = strcat('L1-G-',num2str(i));
end
% Plot goals along with labels
plot(l1.lab1_goals(:,1),l1.lab1_goals(:,2),'r*');
text(l1.lab1_goals(:,1),l1.lab1_goals(:,2),cellstr(labels),'VerticalAlignment','top','HorizontalAlignment','center')
% Create labels for goals in the second lab
n = size(l2.lab2_goals,1);
labels = string(zeros(n,1));
for i = 1:n
    labels(i) = strcat('L2-G-',num2str(i));
end
% Plot goals along with labels
plot(l2.lab2_goals(:,1),l2.lab2_goals(:,2),'g*');
text(l2.lab2_goals(:,1),l2.lab2_goals(:,2),cellstr(labels),'VerticalAlignment','top','HorizontalAlignment','center')
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes goal_planning_viz wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = goal_planning_viz_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure


% --- Executes on selection change in popupmenu5.
function popupmenu5_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu5 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu5



% --- Executes during object creation, after setting all properties.
function popupmenu5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
m = load('modes.mat');
set(hObject,'String',string(m.modes),'Value',1);
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu6.
function popupmenu6_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu6 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu6



% --- Executes during object creation, after setting all properties.
function popupmenu6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
l1 = load('lab1_goals.mat','lab1_goals');
n = size(l1.lab1_goals,1);
labels = string(zeros(n,1));
for i = 1:n
    labels(i) = strcat('L1-G-',num2str(i));
end
set(hObject,'String',labels,'Value',1);
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu7.
function popupmenu7_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu7 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu7

% --- Executes during object creation, after setting all properties.
function popupmenu7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
l1 = load('lab1_goals.mat','lab1_goals');
l2 = load('lab2_goals.mat','lab2_goals');
n2 = size(l2.lab2_goals,1);
n1 = size(l1.lab1_goals,1);
labels = string(zeros(n1+n2,1));
for i = 1:(n1+n2)
    if(i<=n2)
        labels(i) = strcat('L2-G-',num2str(i));
    else
        labels(i) = strcat('L1-G-',num2str(i-n2));
    end
end
set(hObject,'String',labels,'Value',1);
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu8.
function popupmenu8_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu8 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu8


% --- Executes during object creation, after setting all properties.
function popupmenu8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
l1 = load('lab1_goals.mat','lab1_goals');
l2 = load('lab2_goals.mat','lab2_goals');
n2 = size(l2.lab2_goals,1);
n1 = size(l1.lab1_goals,1);
labels = string(zeros(n1+n2,1));
for i = 1:(n1+n2)
    if(i<=n2)
        labels(i) = strcat('L2-G-',num2str(i));
    else
        labels(i) = strcat('L1-G-',num2str(i-n2));
    end
end
set(hObject,'String',labels,'Value',1);
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Save configuration file for each policy
str = char(handles.popupmenu5.String(handles.popupmenu5.Value));
str = strcat(str, '_config');

output.(str).t1.start = lab_position_by_string(handles.popupmenu6.String(handles.popupmenu6.Value));

output.(str).t1.end = lab_position_by_string(handles.popupmenu7.String(handles.popupmenu7.Value));

output.(str).t1.diffGoal = lab_position_by_string(handles.popupmenu8.String(handles.popupmenu8.Value));

output.(str).t2.start = lab_position_by_string(handles.popupmenu10.String(handles.popupmenu10.Value));

output.(str).t2.end = lab_position_by_string(handles.popupmenu11.String(handles.popupmenu11.Value));

output.(str).t2.diffGoal = lab_position_by_string(handles.popupmenu12.String(handles.popupmenu12.Value));

save(strcat(str,'.mat'),'-struct','output',str);

% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close all;

% --- Executes during object creation, after setting all properties.
function axes1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes1


% --- Executes on selection change in popupmenu10.
function popupmenu10_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu10 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu10


% --- Executes during object creation, after setting all properties.
function popupmenu10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
l2 = load('lab2_goals.mat','lab2_goals');
n = size(l2.lab2_goals,1);
labels = string(zeros(n,1));
for i = 1:n
    labels(i) = strcat('L2-G-',num2str(i));
end
set(hObject,'String',labels,'Value',1);
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu11.
function popupmenu11_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu11 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu11


% --- Executes during object creation, after setting all properties.
function popupmenu11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
l1 = load('lab1_goals.mat','lab1_goals');
l2 = load('lab2_goals.mat','lab2_goals');
n2 = size(l2.lab2_goals,1);
n1 = size(l1.lab1_goals,1);
labels = string(zeros(n1+n2,1));
for i = 1:(n1+n2)
    if(i<=n2)
        labels(i) = strcat('L2-G-',num2str(i));
    else
        labels(i) = strcat('L1-G-',num2str(i-n2));
    end
end
set(hObject,'String',labels,'Value',1);
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu12.
function popupmenu12_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu12 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu12


% --- Executes during object creation, after setting all properties.
function popupmenu12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
l1 = load('lab1_goals.mat','lab1_goals');
l2 = load('lab2_goals.mat','lab2_goals');
n2 = size(l2.lab2_goals,1);
n1 = size(l1.lab1_goals,1);
labels = string(zeros(n1+n2,1));
for i = 1:(n1+n2)
    if(i<=n2)
        labels(i) = strcat('L2-G-',num2str(i));
    else
        labels(i) = strcat('L1-G-',num2str(i-n2));
    end
end
set(hObject,'String',labels,'Value',1);
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% lab_position_by_string: helper function for finding a corresponding
% position vector in given labs
% str: string associated with a popup menu
% pos: position vector that corresponds to str
function pos = lab_position_by_string(str)
lab_goal_index_str = char(str);
sub_strings = split(lab_goal_index_str,'-');
lab_name = sub_strings(1);
if strcmp(lab_name, 'L1')
    lab = load('lab1_goals.mat','lab1_goals');
    struct = 'lab1_goals';
else
    lab = load('lab2_goals.mat','lab2_goals');
    struct = 'lab2_goals';
end
index = str2double(sub_strings(3));
pos = lab.(struct)(index,:);