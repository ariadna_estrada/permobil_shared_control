%Joystick move metric 
%From Acollaborative wheelchair system (Qiang Zeng et. al 2008)

%joystick sampled at 20Hz (50 ms between samples)

timestamp = joy_raw(:,1);
joy_change = diff(joy_raw(1:20:end,:)); %sample every second
dt = mean(joy_change(:,1));

mag_change = sqrt(joy_change(:,2).^2 + joy_change(:,3).^2);
joy_movement = mean(mag_change);

%% Joystick jerk defined as the third derivative of position i.e., the rate
%of change of acceleration. 
joy_vel = joy_change(:,2:3) ./ dt;
joy_acc = diff(joy_vel) ./ dt;
joy_jerk = diff(joy_acc) ./ dt;

mag_jerk = sqrt(joy_jerk(:,1).^2 + joy_jerk(:,2).^2);
avg_jerk = mean(mag_jerk);