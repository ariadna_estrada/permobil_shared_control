%% Setup path to trial data
trialNo = 3;
s = what('bagfiles');
trial_directory = strcat(s.path,'/blockB/trial_', num2str(trialNo));

%% Load info to pass to gui
load (fullfile(trial_directory, 'emotion_data.mat'));
load(fullfile(trial_directory, 'trial_data.mat'));
load('lab_20180822.mat');

[n,d] = size(emotion_data);
if d > 9
    emotion_data = emotion_data(:, 1:9);
end
%% Pre-process motion commands. 
%We are only interested in looking at the commands issued when the trial
%was active. So we remove user and planner commands collected outside this
%period.
trial_data.cmd_vel(1,:) = []; %always remove the first one
trial_data.user_weight(1) = [];

first_shared_cmd_time = trial_data.cmd_vel(1,1);
trial_data.joy_user(trial_data.joy_user(:,1) < first_shared_cmd_time,:) = [];
trial_data.eband_vel(trial_data.eband_vel(:,1) < first_shared_cmd_time, :) = [];
%

last_shared_cmd_time = trial_data.cmd_vel(end,1);
trial_data.joy_user(trial_data.joy_user(:,1) > last_shared_cmd_time,:) = [];
trial_data.eband_vel(trial_data.eband_vel(:,1) > last_shared_cmd_time, :) = [];
%

% length(trial_data.cmd_vel)
for i = 2:length(trial_data.cmd_vel)
    t_cmd = trial_data.cmd_vel(i,1);
    ind(i-1) = find(trial_data.joy_user(:,1) < t_cmd, 1, 'last');
end
u_joy = trial_data.joy_user(ind,:);
cmd_vel = trial_data.cmd_vel;
cmd_vel(1,:) = [];

mag_joy = sqrt(u_joy(:,2).^2 + u_joy(:,3).^2);
mag_cmd = sqrt(cmd_vel(:,2).^2 + cmd_vel(:,3).^2);
trial_data.mag_change = (mag_cmd./ mag_joy); 
trial_data.u_joy = u_joy;
%
v_change = u_joy(:,2) - cmd_vel(:,2);
w_change = abs(cmd_vel(:,3) - u_joy(:,3));
%Disagreement in angle
trial_data.disagreement = abs(angdiff(atan2(u_joy(:,3), u_joy(:,2)), atan2(cmd_vel(:,3), cmd_vel(:,2))));
%% Start GUI
close all;
h1 = shared_control_viz(emotion_data, trial_data, trial_directory, map);
h2 = emotion_data_viz(emotion_data, trial_data, trial_directory, map);