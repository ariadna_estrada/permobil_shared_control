function varargout = gazebo_trials(varargin)
% GAZEBO_TRIALS MATLAB code for gazebo_trials.fig
%      GAZEBO_TRIALS, by itself, creates a new GAZEBO_TRIALS or raises the existing
%      singleton*.
%
%      H = GAZEBO_TRIALS returns the handle to a new GAZEBO_TRIALS or the handle to
%      the existing singleton*.
%
%      GAZEBO_TRIALS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GAZEBO_TRIALS.M with the given input arguments.
%
%      GAZEBO_TRIALS('Property','Value',...) creates a new GAZEBO_TRIALS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gazebo_trials_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gazebo_trials_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gazebo_trials

% Last Modified by GUIDE v2.5 09-Mar-2018 16:27:13

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gazebo_trials_OpeningFcn, ...
                   'gui_OutputFcn',  @gazebo_trials_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gazebo_trials is made visible.
function gazebo_trials_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gazebo_trials (see VARARGIN)

% Choose default command line output for gazebo_trials
handles.output = hObject;

handles.NumImg = varargin{1};
handles.face_data = varargin{2};
handles.images_directory = varargin{3};

%Plot face data
set(handles.axes1, 'nextPlot', 'add', 'YLim', [-120 120]);
plot(handles.axes1,  handles.emotion_data, 'LineWidth', 2);
grid on;
xlabel('Seconds'); ylabel('Emotion level');
legend('Joy', 'Fear', 'Disgust', 'Sadness', 'Anger', 'Surprise', 'Contempt', 'Valence','Location', 'northeastoutside');

%Plot first images
img_number = 0;
fullname = fullfile(pwd, handles.images_directory, 'face_images/',['frame' sprintf('%04d',img_number) '.jpg']);
img_rgb = imread(fullname);
imshow(img_rgb, 'Parent', handles.axes2);
fullname = fullfile(pwd, handles.images_directory, 'kinect_pov/',['frame' sprintf('%04d',img_number) '.jpg']);
img_rgb = imread(fullname);
imshow(img_rgb, 'Parent', handles.axes3);


% Update handles structure
guidata(hObject, handles);

% UIWAIT makes gazebo_trials wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = gazebo_trials_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.slider_value = get(hObject,'Value');
img_number = floor(handles.slider_value*handles.num_img);
fullname = fullfile(pwd, 'extracted_images/trial1', ['frame' sprintf('%04d',img_number) '.jpg']);
img_rgb = imread(fullname);
imshow(img_rgb, 'Parent', handles.axes2);

guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
handles.slider_value = get(hObject,'Value');
guidata(hObject, handles);


% --- Executes on mouse press over axes background.
function axes1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ax_point = ginput(1);
% ax_point = get(hObject,'currentpoint')
ax_point = ax_point(1) / handles.axes1.XLim(2);

img_number = floor(ax_point*handles.NumImg.face);
fullname = fullfile(pwd, handles.images_directory, 'face_images/',['frame' sprintf('%04d',img_number) '.jpg']);
img_rgb = imread(fullname);
imshow(img_rgb, 'Parent', handles.axes2);


img_number = floor(ax_point*handles.NumImg.kinect);
fullname = fullfile(pwd, handles.images_directory, 'kinect_pov/',['frame' sprintf('%04d',img_number) '.jpg']);
img_rgb = imread(fullname);
imshow(img_rgb, 'Parent', handles.axes3);

guidata(hObject, handles);

function axes1_CreateFcn(hObject, eventdata, handles)
