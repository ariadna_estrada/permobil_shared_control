wheelchair-simulation
=====================

Code for simulating a PERMOBIL wheelchair and performing mapping and localization tasks.

Package Descriptions
---------------------
config: parm files for the move_base framework

launch: launch files

rviz: configuration files for rviz


Useful launch files
---------------------

Gazebo (Only for simulation): wheelchair_spawn.launch

Creating a map Launch Order:

1. roslaunch wheelchair_simulation wheelchair_gazebo_mapping.launch
2. roslaunch ira_laser_tools wheelchair_scan_merger.launch

Testing wheelchair localization Launch Order:

1. roslaunch wheelchair_simulation wheelchair_gazebo_localization.launch
2. roslaunch ira_laser_tools wheelchair_scan_merger.launch

Testing autonomous planner (elastic band) Launch Order:

1. roslaunch wheelchair_simulation wheelchair_gazebo_eband.launch
2. roslaunch ira_laser_tools wheelchair_scan_merger.launch


Dependancies
---------------------
+ ros-indigo-scan-tools   
+ ros-indigo-move-base  

ira_laser_tools
---------------------
1. cd [indigo catkin workspace directory]  
2. git clone https://github.com/iralabdisco/ira_laser_tools.git  
4. catkin_make  
5. roslaunch ira_laser_tools laserscan_multi_merger.launch

